import {reducer} from './src/common/reducer';
import UFSKnowledgeBaseContainer from './src/screens/app';
import {configure} from './src/services/api/ecm/config';


const reducers = {
    knowledgeBase: reducer
};


export {
    UFSKnowledgeBaseContainer,
    reducers,
    configure
};
