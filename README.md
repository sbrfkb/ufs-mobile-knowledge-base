
# ufs-mobile-knowledge-base

## Getting started

`$ npm install ufs-mobile-knowledge-base --save`

### Mostly automatic installation

`$ react-native link ufs-mobile-knowledge-base`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `ufs-mobile-knowledge-base` and add `UFSMobileKnowledgeBase.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libUFSMobileKnowledgeBase.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<


## Usage
```javascript
import UFSMobileKnowledgeBase from 'ufs-mobile-ufs-mobile-knowledge-base';

// TODO: What to do with the module?
UFSMobileKnowledgeBase;
```
