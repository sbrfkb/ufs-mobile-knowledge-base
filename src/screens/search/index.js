import { connect } from 'react-redux';
import { select } from '../../data/tree/index';
import { ExternalDocument } from '../../data/documents/models';
import { openExternalDocumentScreen } from '../document/external/actions';
import Component from './component';
import { groupedResults, mapResults } from './selectors';
const mapStateToProps = (state) => {
    const searchState = state.user.knowledgeBase.screens.search;
    const documentsState = state.user.knowledgeBase.data.documents;
    return {
        fetching: searchState.fetching,
        searchString: searchState.searchString,
        documents: mapResults(documentsState.itemsByID, searchState.documentsIDs),
        groupedResults: groupedResults(documentsState.itemsByID, searchState.documentsIDs)
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        showDocument: (document) => {
            if (document instanceof ExternalDocument) {
                dispatch(openExternalDocumentScreen(document));
            }
            else {
                dispatch(select(document));
            }
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Component);
//# sourceMappingURL=index.js.map