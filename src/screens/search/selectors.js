export const mapResults = (itemsByID, ids) => {
    if (ids === undefined) {
        return [];
    }
    return ids.map((documentID) => {
        return itemsByID.get(documentID);
    });
};
export const groupedResults = (itemsByID, ids) => {
    if (ids === undefined) {
        return [];
    }
    // Группируем документы по родительской группе
    let documentsByGroupID = new Map();
    ids.forEach((documentID) => {
        let document = itemsByID.get(documentID);
        let groupDocuments = documentsByGroupID.get(document.parentID);
        if (groupDocuments === undefined) {
            groupDocuments = [];
        }
        groupDocuments.push(document);
        documentsByGroupID.set(document.parentID, groupDocuments);
    });
    // Соединяем информацию о группе и найденых документах
    let groupedResults = [];
    documentsByGroupID.forEach((documents, groupID) => {
        groupedResults.push({
            group: itemsByID.get(groupID),
            documents: documents
        });
    });
    return groupedResults;
};
//# sourceMappingURL=selectors.js.map