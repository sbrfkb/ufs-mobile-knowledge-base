import React, { Component } from 'react';
import { ActivityIndicator, Image, ListView, TouchableHighlight } from 'react-native';
import { Text, View } from 'ufs-mobile-platform';
import { ExternalDocument } from '../../data/documents/models';
import { colors } from '../../common/constants';
import styles from './styles';
var SearchPageType;
(function (SearchPageType) {
    SearchPageType[SearchPageType["suggest"] = 0] = "suggest";
    SearchPageType[SearchPageType["results"] = 1] = "results";
})(SearchPageType || (SearchPageType = {}));
export default class StaticDocumentComponent extends Component {
    constructor(props) {
        super(props);
        this._renderSuggestPage = () => {
            return (React.createElement(View, { style: styles.container },
                React.createElement(TouchableHighlight, { onPress: () => this.setState({ type: SearchPageType.results }), underlayColor: colors.grayBG },
                    React.createElement(View, { style: styles.showResultsButton },
                        React.createElement(Text, { style: styles.showResultsButtonTitle, numberOfLines: 1 }, `Искать "${this.props.searchString}"`))),
                React.createElement(ListView, { dataSource: this.state.plainDataSource.cloneWithRows(this.props.documents), renderRow: this._renderRow, renderSeparator: this._renderSeparator })));
        };
        this._renderResultsPage = () => {
            let dataBlob = {};
            let sectionsIDs = [];
            let rowsIDs = [];
            this.props.groupedResults.forEach((result, sectionID) => {
                sectionsIDs.push(result.group.id);
                dataBlob[result.group.id] = result.group;
                rowsIDs[sectionID] = [];
                result.documents.forEach((document) => {
                    rowsIDs[sectionID].push(document.id);
                    dataBlob[`${result.group.id}:${document.id}`] = document;
                });
            });
            return (React.createElement(ListView, { dataSource: this.state.sectionDataSource.cloneWithRowsAndSections(dataBlob, sectionsIDs, rowsIDs), renderRow: this._renderRow, renderSeparator: this._renderSeparator, renderSectionHeader: this._renderSectionHeader, enableEmptySections: true }));
        };
        this._renderRow = (document) => {
            return (React.createElement(TouchableHighlight, { underlayColor: colors.highlight, onPress: () => {
                    this.props.showDocument(document);
                } },
                React.createElement(View, { style: styles.rowContainer },
                    React.createElement(View, { style: [styles.rowTypeContainer, { backgroundColor: document instanceof ExternalDocument ? colors.orange : colors.green }] },
                        React.createElement(Text, { style: styles.rowTypeTitle }, document instanceof ExternalDocument ? 'PDF' : 'READ')),
                    React.createElement(Text, { style: styles.rowTitle, numberOfLines: 1 }, document.title),
                    React.createElement(Image, { source: require('./images/arrow.png'), style: styles.arrowImage }))));
        };
        this._renderSeparator = (sectionID, rowID) => {
            return (React.createElement(View, { key: `${sectionID}-${rowID}`, style: styles.separator }));
        };
        this._renderSectionHeader = (group) => {
            return (React.createElement(View, { style: styles.sectionContainer },
                React.createElement(Text, { style: styles.sectionTitle }, group.title)));
        };
        this._renderActivityIndicator = () => {
            return (React.createElement(View, { style: styles.container },
                React.createElement(View, { style: styles.emptyTextContainer },
                    React.createElement(ActivityIndicator, { animating: true, size: 'large', style: styles.indicator }))));
        };
        this._renderEmptyText = (text) => {
            return (React.createElement(View, { style: styles.emptyTextContainer },
                React.createElement(Text, { style: styles.emptyText }, text)));
        };
        this.state = {
            type: SearchPageType.suggest,
            plainDataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            }),
            sectionDataSource: new ListView.DataSource({
                getRowData: (data, sectionID, rowID) => {
                    return data[`${sectionID}:${rowID}`];
                },
                getSectionHeaderData: (data, sectionID) => {
                    return data[sectionID];
                },
                rowHasChanged: (r1, r2) => r1 !== r2,
                sectionHeaderHasChanged: (s1, s2) => s1 !== s2
            })
        };
    }
    componentWillReceiveProps(props) {
        if (props.isSearchBarActive) {
            this.setState({ type: SearchPageType.suggest });
        }
    }
    render() {
        if (this.props.fetching) {
            return this._renderActivityIndicator();
        }
        else if (!this.props.searchString || this.props.searchString.length == 0) {
            return this._renderEmptyText('Поиск по всей базе знаний');
        }
        else if (this.props.documents.length == 0) {
            return this._renderEmptyText('Ничего не найдено');
        }
        else {
            switch (this.state.type) {
                case SearchPageType.suggest:
                    return this._renderSuggestPage();
                case SearchPageType.results:
                default:
                    return this._renderResultsPage();
            }
        }
    }
}
//# sourceMappingURL=component.js.map