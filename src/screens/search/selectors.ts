import {Document} from '../../data/documents/models';
import {ITreeItem} from '../../data/tree/models';

import {SearchResultGroup} from './models';


export const mapResults = (itemsByID: Map<string, ITreeItem>, ids: string[]) => {
    if (ids === undefined) {
        return [];
    }

    return ids.map((documentID) => {
        return itemsByID.get(documentID) as Document;
    });
};


export const groupedResults = (itemsByID: Map<string, ITreeItem>, ids: string[]) => {
    if (ids === undefined) {
        return [];
    }

    // Группируем документы по родительской группе
    let documentsByGroupID = new Map<string, Document[]>();
    ids.forEach((documentID) => {
        let document = itemsByID.get(documentID) as Document;

        let groupDocuments = documentsByGroupID.get(document.parentID);
        if (groupDocuments === undefined) {
            groupDocuments = [];
        }
        groupDocuments.push(document);
        documentsByGroupID.set(document.parentID, groupDocuments);
    });

    // Соединяем информацию о группе и найденых документах
    let groupedResults: SearchResultGroup[] = [];
    documentsByGroupID.forEach((documents, groupID) => {
        groupedResults.push({
            group: itemsByID.get(groupID),
            documents: documents
        });
    });

    return groupedResults;
};
