import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../common/constants';
export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    emptyTextContainer: {
        height: 300,
        alignItems: 'center',
        justifyContent: 'center'
    },
    emptyText: {
        color: colors.placeholder
    },
    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    },
    // Show results
    showResultsButton: {
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: colors.border
    },
    showResultsButtonTitle: {
        fontFamily: fonts.bold,
        fontSize: 15
    },
    // Document row
    rowContainer: {
        marginHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        height: 40
    },
    rowTypeContainer: {
        paddingHorizontal: 3,
        paddingVertical: 1,
        borderRadius: 3,
        height: 14,
        width: 30,
        alignItems: 'center'
    },
    rowTypeTitle: {
        fontFamily: fonts.bold,
        fontSize: 8,
        color: 'white'
    },
    rowTitle: {
        marginLeft: 10,
        fontFamily: fonts.regular,
        fontSize: 16,
        flex: 1,
    },
    documentRowOtherInfo: {
        fontFamily: fonts.regular,
        fontSize: 12,
        color: colors.placeholder
    },
    // Section
    sectionContainer: {
        paddingTop: 20,
        paddingBottom: 10,
        paddingHorizontal: 20,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: colors.border
    },
    sectionTitle: {
        fontFamily: fonts.regular,
        fontSize: 12,
        color: colors.placeholder
    },
    arrowImage: {
        width: 7,
        height: 12
    },
    separator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: colors.border,
        marginLeft: 20
    }
});
//# sourceMappingURL=styles.js.map