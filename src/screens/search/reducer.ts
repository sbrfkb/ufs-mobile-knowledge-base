import {IAction} from '../../common/models';

import * as treeActionTypes from '../../data/tree/actionTypes';

import * as actionTypes from './actionTypes';
import {ISearchScreenState} from './models';


const initialState = {
    isOpen: false,
    fetching: false
} as ISearchScreenState;


export const reducer = (state = initialState, action: IAction): ISearchScreenState => {
    switch (action.type) {

        case actionTypes.OPEN:
            return {
                ...state,
                isOpen: true
            };

        case actionTypes.CLOSE:
        case treeActionTypes.SELECT:
            return {
                ...state,
                isOpen: false
            };

        case actionTypes.UPDATE: {
            if (state.searchString === action.payload.searchString) {
                return {
                    ...state,
                    documentsIDs: action.payload.documentsIDs
                };
            } else {
                return state;
            }
        }

        case actionTypes.REQUEST.BEGIN: {
            return {
                ...state,
                fetching: true,
                searchString: action.payload.searchString
            };
        }

        case actionTypes.REQUEST.END: {
            if (state.searchString === action.payload.searchString) {
                return {
                    ...state,
                    fetching: false
                };
            } else {
                return state;
            }
        }

        default:
            return state;

    }
};
