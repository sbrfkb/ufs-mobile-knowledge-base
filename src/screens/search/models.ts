import {Group} from '../../data/tree/models';
import {Document} from '../../data/documents/models';


export interface ISearchScreenState {
    isOpen?: boolean
    fetching?: boolean
    error?: Error
    searchString?: string
    documentsIDs?: string[]
}

export interface SearchResultGroup {
    group: Group;
    documents: Document[];
}
