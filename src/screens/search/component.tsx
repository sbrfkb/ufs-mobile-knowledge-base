import React, {Component} from 'react';
import {ActivityIndicator, Image, ListView, TouchableHighlight} from 'react-native';
import {Text, View} from 'ufs-mobile-platform';

import {Document, ExternalDocument} from '../../data/documents/models';
import {colors} from '../../common/constants';
import {Group} from '../../data/tree/models';

import {SearchResultGroup} from './models';
import styles from './styles';


enum SearchPageType {
    suggest,
    results,
}


export interface IProps {
    isSearchBarActive?: boolean

    fetching?: boolean
    searchString?: string
    documents?: Document[]
    groupedResults?: SearchResultGroup[]

    showDocument?: (document: Document) => void
}


interface IState {
    type: SearchPageType
    plainDataSource: React.ListViewDataSource
    sectionDataSource: React.ListViewDataSource
}


export default class StaticDocumentComponent extends Component<IProps, IState> {

    componentWillReceiveProps(props: IProps) {
        if (props.isSearchBarActive) {
            this.setState({type: SearchPageType.suggest});
        }
    }

    constructor(props: IProps) {
        super(props);

        this.state = {
            type: SearchPageType.suggest,
            plainDataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            }),
            sectionDataSource: new ListView.DataSource({
                getRowData: (data: any, sectionID: number, rowID: number) => {
                    return data[`${sectionID}:${rowID}`];
                },
                getSectionHeaderData: (data: any, sectionID: number) => {
                    return data[sectionID];
                },
                rowHasChanged: (r1, r2) => r1 !== r2,
                sectionHeaderHasChanged: (s1, s2) => s1 !== s2
            })
        };
    }

    render() {
        if (this.props.fetching) {
            return this._renderActivityIndicator();
        } else if (!this.props.searchString || this.props.searchString.length == 0) {
            return this._renderEmptyText('Поиск по всей базе знаний');
        } else if (this.props.documents.length == 0) {
            return this._renderEmptyText('Ничего не найдено');
        } else {
            switch (this.state.type) {
                case SearchPageType.suggest:
                    return this._renderSuggestPage();
                case SearchPageType.results:
                default:
                    return this._renderResultsPage();
            }
        }
    }

    private _renderSuggestPage = () => {
        return (
            <View style={styles.container}>
                <TouchableHighlight onPress={() => this.setState({type: SearchPageType.results})} underlayColor={colors.grayBG}>
                    <View style={styles.showResultsButton}>
                        <Text style={styles.showResultsButtonTitle} numberOfLines={1}>
                            {`Искать "${this.props.searchString}"`}
                        </Text>
                    </View>
                </TouchableHighlight>
                <ListView
                    dataSource={this.state.plainDataSource.cloneWithRows(this.props.documents)}
                    renderRow={this._renderRow}
                    renderSeparator={this._renderSeparator}
                />
            </View>
        );
    };

    private _renderResultsPage = () => {
        let dataBlob: any = {};

        let sectionsIDs: string[] = [];
        let rowsIDs: string[][] = [];

        this.props.groupedResults.forEach((result, sectionID) => {
            sectionsIDs.push(result.group.id);
            dataBlob[result.group.id] = result.group;

            rowsIDs[sectionID] = [];

            result.documents.forEach((document) => {
                rowsIDs[sectionID].push(document.id);
                dataBlob[`${result.group.id}:${document.id}`] = document;
            });
        });

        return (
            <ListView
                dataSource={this.state.sectionDataSource.cloneWithRowsAndSections(dataBlob, sectionsIDs, rowsIDs)}
                renderRow={this._renderRow}
                renderSeparator={this._renderSeparator}
                renderSectionHeader={this._renderSectionHeader}
                enableEmptySections={true}
            />
        );
    };

    private _renderRow = (document: Document) => {
        return (
            <TouchableHighlight underlayColor={colors.highlight} onPress={() => {
                this.props.showDocument(document);
            }}>
                <View style={styles.rowContainer}>
                    <View
                        style={[styles.rowTypeContainer, {backgroundColor: document instanceof ExternalDocument ? colors.orange : colors.green}]}>
                        <Text style={styles.rowTypeTitle}>
                            {document instanceof ExternalDocument ? 'PDF' : 'READ'}
                        </Text>
                    </View>
                    <Text style={styles.rowTitle} numberOfLines={1}>
                        {document.title}
                    </Text>

                    <Image
                        source={require('./images/arrow.png')}
                        style={styles.arrowImage}
                    />
                </View>
            </TouchableHighlight>
        );
    };

    private _renderSeparator = (sectionID: React.ReactText, rowID: React.ReactText) => {
        return (
            <View
                key={`${sectionID}-${rowID}`}
                style={styles.separator}
            />
        );
    };

    private _renderSectionHeader = (group: Group) => {
        return (
            <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>{group.title}</Text>
            </View>
        );
    };

    private _renderActivityIndicator = () => {
        return (
            <View style={styles.container}>
                <View style={styles.emptyTextContainer}>
                    <ActivityIndicator
                        animating={true}
                        size={'large'}
                        style={styles.indicator}
                    />
                </View>
            </View>
        );
    };

    private _renderEmptyText = (text: string) => {
        return (
            <View style={styles.emptyTextContainer}>
                <Text style={styles.emptyText}>
                    {text}
                </Text>
            </View>
        );
    };

}
