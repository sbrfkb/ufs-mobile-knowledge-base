import {IGlobalState} from '../../common/models';
import {ITreeItem} from '../../data/tree/models';
import {Document} from '../../data/documents/models';

import * as actionTypes from './actionTypes';


export const openSearchScreen = () => ({
    type: actionTypes.OPEN
});

export const closeSearchScreen = () => ({
    type: actionTypes.CLOSE
});

export const search = (searchString: string) => {
    const request = {
        begin: (searchString) => ({
            type: actionTypes.REQUEST.BEGIN,
            payload: {
                searchString
            }
        }),
        end: (searchString) => ({
            type: actionTypes.REQUEST.END,
            payload: {
                searchString
            }
        })
    };

    const update = (searchString: string, documentsIDs: string[]) => ({
        type: actionTypes.UPDATE,
        payload: {
            searchString,
            documentsIDs
        }
    });

    return (dispatch, getState) => {
        dispatch(request.begin(searchString));

        const state: IGlobalState = getState();
        const documentsIDs: string[] = [];
        if (searchString.length > 0) {
            state.user.knowledgeBase.data.documents.itemsByID.forEach((item: ITreeItem) => {
                if (item instanceof Document && item.title.toLowerCase().indexOf(searchString.toLowerCase()) !== -1) {
                    documentsIDs.push(item.id);
                }
            });
        }

        dispatch(update(searchString, documentsIDs));
        dispatch(request.end(searchString));
    };
};
