import * as treeActionTypes from '../../data/tree/actionTypes';
import * as actionTypes from './actionTypes';
const initialState = {
    isOpen: false,
    fetching: false
};
export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.OPEN:
            return Object.assign({}, state, { isOpen: true });
        case actionTypes.CLOSE:
        case treeActionTypes.SELECT:
            return Object.assign({}, state, { isOpen: false });
        case actionTypes.UPDATE: {
            if (state.searchString === action.payload.searchString) {
                return Object.assign({}, state, { documentsIDs: action.payload.documentsIDs });
            }
            else {
                return state;
            }
        }
        case actionTypes.REQUEST.BEGIN: {
            return Object.assign({}, state, { fetching: true, searchString: action.payload.searchString });
        }
        case actionTypes.REQUEST.END: {
            if (state.searchString === action.payload.searchString) {
                return Object.assign({}, state, { fetching: false });
            }
            else {
                return state;
            }
        }
        default:
            return state;
    }
};
//# sourceMappingURL=reducer.js.map