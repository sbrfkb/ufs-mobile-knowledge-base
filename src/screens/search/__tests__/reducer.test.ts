import {reducer} from '../reducer';
import * as actionTypes from '../actionTypes';


describe('search:reducer', () => {

    it('должен возвращать начальный стейт', () => {
        const action = {
            type: 'test action'
        };
        const initialState = {
            isOpen: false,
            fetching: false
        };

        expect(reducer(initialState, action)).toEqual(initialState);
    });

    it('OPEN', () => {
        const initialState = {
            isOpen: false,
            fetching: false
        };
        const action = {
            type: actionTypes.OPEN
        };
        const expectedState = {
            isOpen: true,
            fetching: false
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('CLOSE', () => {
        const initialState = {
            isOpen: true,
            fetching: false
        };
        const action = {
            type: actionTypes.CLOSE
        };
        const expectedState = {
            isOpen: false,
            fetching: false
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    describe('REQUEST', () => {

        it('BEGIN', () => {
            const initialState = {
                isOpen: true,
                fetching: false,
                searchString: 'test'
            };
            const action = {
                type: actionTypes.REQUEST.BEGIN,
                payload: {
                    searchString: 'test1'
                }
            };
            const expectedState = {
                isOpen: true,
                fetching: true,
                searchString: 'test1'
            };

            expect(reducer(initialState, action)).toEqual(expectedState);
        });

        describe('правильная поисковая строка', () => {

            it('END', () => {
                const initialState = {
                    isOpen: true,
                    fetching: true,
                    searchString: 'test'
                };
                const action = {
                    type: actionTypes.REQUEST.END,
                    payload: {
                        searchString: 'test'
                    }
                };
                const expectedState = {
                    isOpen: true,
                    fetching: false,
                    searchString: 'test'
                };

                expect(reducer(initialState, action)).toEqual(expectedState);
            });

        });

        describe('неправильная поисковая строка', () => {

            it('END', () => {
                const initialState = {
                    isOpen: true,
                    fetching: true,
                    searchString: 'test'
                };
                const action = {
                    type: actionTypes.REQUEST.END,
                    payload: {
                        searchString: 'test1'
                    }
                };

                expect(reducer(initialState, action)).toEqual(initialState);
            });

        });

    });

    describe('UPDATE', () => {

        it('правильная поисковая строка', () => {
            const initialState = {
                isOpen: true,
                fetching: true,
                searchString: 'test',
                documentsIDs: ['2']
            };
            const action = {
                type: actionTypes.UPDATE,
                payload: {
                    searchString: 'test',
                    documentsIDs: ['1', '2']
                }
            };
            const expectedState = {
                isOpen: true,
                fetching: true,
                searchString: 'test',
                documentsIDs: ['1', '2']
            };

            expect(reducer(initialState, action)).toEqual(expectedState);

        });

        it('неправильная поисковая строка', () => {
            const initialState = {
                isOpen: true,
                fetching: true,
                searchString: 'test',
                documentsIDs: ['2']
            };
            const action = {
                type: actionTypes.UPDATE,
                payload: {
                    searchString: 'test1',
                    documentsIDs: ['1', '2']
                }
            };

            expect(reducer(initialState, action)).toEqual(initialState);
        });

    });
});
