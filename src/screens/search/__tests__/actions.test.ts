import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import * as actions from '../actions';
import * as actionTypes from '../actionTypes';
import {Group, ITreeItem} from '../../../data/tree/models';
import {ExternalDocument, StaticDocument} from '../../../data/documents/models';


const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);


describe('search:openSearchScreen', () => {
    it('посылает правильный экшн', () => {
        const expectedAction = {
            type: actionTypes.OPEN
        };
        expect(actions.openSearchScreen()).toEqual(expectedAction);
    });
});

describe('search:closeSearchScreen', () => {
    it('посылает правильный экшн', () => {
        const expectedAction = {
            type: actionTypes.CLOSE
        };
        expect(actions.closeSearchScreen()).toEqual(expectedAction);
    });
});

describe('search:search', () => {

    const createTest = (searchString, documentsByID, documentsIDs) => {
        const expectedActions = [
            {type: actionTypes.REQUEST.BEGIN, payload: {searchString}},
            {type: actionTypes.UPDATE, payload: {searchString, documentsIDs}},
            {type: actionTypes.REQUEST.END, payload: {searchString}}
        ];
        const store = mockStore({
            user: {
                knowledgeBase: {
                    data: {
                        documents: {
                            itemsByID: documentsByID
                        }
                    }
                }
            }
        });

        store.dispatch(actions.search(searchString));
        expect(store.getActions()).toEqual(expectedActions);
    };

    it('когда нет документов', () => {
        const searchString = 'test';
        const documentsByID = new Map<string, ITreeItem>();
        const documentsIDs = [];

        createTest(searchString, documentsByID, documentsIDs);
    });

    it('когда нет найденного', () => {
        const searchString = 'test';
        const documentsByID = new Map<string, ITreeItem>();

        const document1PDF = new ExternalDocument();
        document1PDF.id = 'dp1';
        document1PDF.path = 'document1.pdf';
        document1PDF.title = 'Document 1';
        documentsByID.set(document1PDF.id, document1PDF);

        const document2TXT = new StaticDocument();
        document2TXT.id = 'd2';
        document2TXT.path = 'document2.txt';
        document2TXT.title = 'Document 2';
        documentsByID.set(document2TXT.id, document2TXT);

        const group1 = new Group();
        group1.id = '1';
        group1.path = 'group1';
        group1.title = 'Group 1';
        documentsByID.set(group1.id, group1);

        const documentsIDs = [];

        createTest(searchString, documentsByID, documentsIDs);
    });

    it('не должен искать папки', () => {
        const searchString = 'Group';
        const documentsByID = new Map<string, ITreeItem>();

        const document1PDF = new ExternalDocument();
        document1PDF.id = 'dp1';
        document1PDF.path = 'document1.pdf';
        document1PDF.title = 'Document 1';
        documentsByID.set(document1PDF.id, document1PDF);

        const document2TXT = new StaticDocument();
        document2TXT.id = 'd2';
        document2TXT.path = 'document2.txt';
        document2TXT.title = 'Document 2';
        documentsByID.set(document2TXT.id, document2TXT);

        const group1 = new Group();
        group1.id = '1';
        group1.path = 'group1';
        group1.title = 'Group 1';
        documentsByID.set(group1.id, group1);

        const documentsIDs = [];

        createTest(searchString, documentsByID, documentsIDs);
    });

    it('должен искать по части фразы', () => {
        const searchString = 'ment';
        const documentsByID = new Map<string, ITreeItem>();

        const document1PDF = new ExternalDocument();
        document1PDF.id = 'dp1';
        document1PDF.path = 'document1.pdf';
        document1PDF.title = 'Document 1';
        documentsByID.set(document1PDF.id, document1PDF);

        const document2TXT = new StaticDocument();
        document2TXT.id = 'd2';
        document2TXT.path = 'document2.txt';
        document2TXT.title = 'Document 2';
        documentsByID.set(document2TXT.id, document2TXT);

        const group1 = new Group();
        group1.id = '1';
        group1.path = 'group1';
        group1.title = 'Group 1';
        documentsByID.set(group1.id, group1);

        const documentsIDs = [
            document1PDF.id,
            document2TXT.id
        ];

        createTest(searchString, documentsByID, documentsIDs);
    });

    it('должен искать без учета регистра', () => {
        const searchString = 'mENt';
        const documentsByID = new Map<string, ITreeItem>();

        const document1PDF = new ExternalDocument();
        document1PDF.id = 'dp1';
        document1PDF.path = 'document1.pdf';
        document1PDF.title = 'Document 1';
        documentsByID.set(document1PDF.id, document1PDF);

        const document2TXT = new StaticDocument();
        document2TXT.id = 'd2';
        document2TXT.path = 'document2.txt';
        document2TXT.title = 'Document 2';
        documentsByID.set(document2TXT.id, document2TXT);

        const group1 = new Group();
        group1.id = '1';
        group1.path = 'group1';
        group1.title = 'Group 1';
        documentsByID.set(group1.id, group1);

        const documentsIDs = [
            document1PDF.id,
            document2TXT.id
        ];

        createTest(searchString, documentsByID, documentsIDs);
    });

    it('должен искать только по названию документа', () => {
        const searchString = 'txt';
        const documentsByID = new Map<string, ITreeItem>();

        const document1PDF = new ExternalDocument();
        document1PDF.id = 'dp1';
        document1PDF.path = 'document1.pdf';
        document1PDF.title = 'Document 1';
        documentsByID.set(document1PDF.id, document1PDF);

        const document2TXT = new StaticDocument();
        document2TXT.id = 'd2';
        document2TXT.path = 'document2.txt';
        document2TXT.title = 'Document 2';
        documentsByID.set(document2TXT.id, document2TXT);

        const group1 = new Group();
        group1.id = '1';
        group1.path = 'group1';
        group1.title = 'Group 1';
        documentsByID.set(group1.id, group1);

        const documentsIDs = [];

        createTest(searchString, documentsByID, documentsIDs);
    });

    it('пустая поисковая строка', () => {
        const searchString = '';
        const documentsByID = new Map<string, ITreeItem>();

        const document1PDF = new ExternalDocument();
        document1PDF.id = 'dp1';
        document1PDF.path = 'document1.pdf';
        document1PDF.title = 'Document 1';
        documentsByID.set(document1PDF.id, document1PDF);

        const document2TXT = new StaticDocument();
        document2TXT.id = 'd2';
        document2TXT.path = 'document2.txt';
        document2TXT.title = 'Document 2';
        documentsByID.set(document2TXT.id, document2TXT);

        const group1 = new Group();
        group1.id = '1';
        group1.path = 'group1';
        group1.title = 'Group 1';
        documentsByID.set(group1.id, group1);

        const documentsIDs = [];

        createTest(searchString, documentsByID, documentsIDs);
    });

});
