import { groupedResults, mapResults } from '../selectors';
import { Group } from '../../../data/tree/models';
import { ExternalDocument, StaticDocument } from '../../../data/documents/models';
describe('search:selectors', () => {
    it('mapResults', () => {
        const documentsByID = new Map();
        const document1PDF = new ExternalDocument();
        document1PDF.id = 'dp1';
        document1PDF.path = 'document1.pdf';
        document1PDF.title = 'Document 1';
        documentsByID.set(document1PDF.id, document1PDF);
        const document2TXT = new StaticDocument();
        document2TXT.id = 'd2';
        document2TXT.path = 'document2.txt';
        document2TXT.title = 'Document 2';
        documentsByID.set(document2TXT.id, document2TXT);
        const group1 = new Group();
        group1.id = '1';
        group1.path = 'group1';
        group1.title = 'Group 1';
        documentsByID.set(group1.id, group1);
        expect(mapResults(documentsByID, [document2TXT.id, document1PDF.id])).toEqual([document2TXT, document1PDF]);
        expect(mapResults(documentsByID, [document1PDF.id])).toEqual([document1PDF]);
        expect(mapResults(documentsByID, [])).toEqual([]);
        expect(mapResults(documentsByID, undefined)).toEqual([]);
    });
    it('groupedResults', () => {
        const documentsByID = new Map();
        const group1 = new Group();
        group1.id = '1';
        group1.path = 'group1';
        group1.title = 'Group 1';
        documentsByID.set(group1.id, group1);
        const document1PDF = new ExternalDocument();
        document1PDF.id = 'dp1';
        document1PDF.path = 'document1.pdf';
        document1PDF.title = 'Document 1';
        document1PDF.parentID = group1.id;
        documentsByID.set(document1PDF.id, document1PDF);
        const document2TXT = new StaticDocument();
        document2TXT.id = 'd2';
        document2TXT.path = 'document2.txt';
        document2TXT.title = 'Document 2';
        document2TXT.parentID = group1.id;
        documentsByID.set(document2TXT.id, document2TXT);
        const group2 = new Group();
        group2.id = 'g2';
        group2.path = 'group2';
        group2.title = 'Group 2';
        documentsByID.set(group2.id, group2);
        const document3PDF = new ExternalDocument();
        document3PDF.id = 'dp3';
        document3PDF.path = 'document3.pdf';
        document3PDF.title = 'Document 3';
        document3PDF.parentID = group2.id;
        documentsByID.set(document3PDF.id, document3PDF);
        expect(groupedResults(documentsByID, [document2TXT.id, document1PDF.id])).toEqual([
            {
                group: group1,
                documents: [document2TXT, document1PDF]
            }
        ]);
        expect(groupedResults(documentsByID, [document3PDF.id, document1PDF.id])).toEqual([
            {
                group: group2,
                documents: [document3PDF]
            },
            {
                group: group1,
                documents: [document1PDF]
            }
        ]);
        expect(groupedResults(documentsByID, [])).toEqual([]);
        expect(groupedResults(documentsByID, undefined)).toEqual([]);
    });
});
//# sourceMappingURL=selectors.test.js.map