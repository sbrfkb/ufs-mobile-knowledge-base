export const OPEN = 'KB/SEARCH/OPEN';
export const CLOSE = 'KB/SEARCH/CLOSE';
export const UPDATE = 'KB/SEARCH/UPDATE';

export const REQUEST = {
    BEGIN: 'KB/SEARCH/REQUEST/BEGIN',
    END: 'KB/SEARCH/REQUEST/END'
};
