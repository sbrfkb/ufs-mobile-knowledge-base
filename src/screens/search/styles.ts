import React from 'react';
import {StyleSheet} from 'react-native';
import {colors, fonts} from '../../common/constants';


export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white'
    } as React.ViewStyle,

    emptyTextContainer: {
        height: 300,
        alignItems: 'center',
        justifyContent: 'center'
    } as React.ViewStyle,

    emptyText: {
        color: colors.placeholder
    } as React.TextStyle,

    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    } as React.ViewStyle,

    // Show results

    showResultsButton: {
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: colors.border
    } as React.ViewStyle,

    showResultsButtonTitle: {
        fontFamily: fonts.bold,
        fontSize: 15
    } as React.TextStyle,


    // Document row
    rowContainer: {
        marginHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        height: 40
    } as React.ViewStyle,

    rowTypeContainer: {
        paddingHorizontal: 3,
        paddingVertical: 1,
        borderRadius: 3,
        height: 14,
        width: 30,
        alignItems: 'center'
    } as React.ViewStyle,

    rowTypeTitle: {
        fontFamily: fonts.bold,
        fontSize: 8,
        color: 'white'
    } as React.TextStyle,

    rowTitle: {
        marginLeft: 10,
        fontFamily: fonts.regular,
        fontSize: 16,
        flex: 1,
    } as React.TextStyle,

    documentRowOtherInfo: {
        fontFamily: fonts.regular,
        fontSize: 12,
        color: colors.placeholder
    } as React.TextStyle,


    // Section

    sectionContainer: {
        paddingTop: 20,
        paddingBottom: 10,
        paddingHorizontal: 20,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: colors.border
    } as React.ViewStyle,

    sectionTitle: {
        fontFamily: fonts.regular,
        fontSize: 12,
        color: colors.placeholder
    } as React.TextStyle,


    arrowImage: {
        width: 7,
        height: 12
    } as React.ImageStyle,

    separator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: colors.border,
        marginLeft: 20
    } as React.ViewStyle

});
