import {ISearchScreenState} from './search/models';
import {IExternalDocumentState} from './document/external/models';


export interface IScreensState {
    search: ISearchScreenState
    externalDocument: IExternalDocumentState
}
