import {IAction} from '../../../common/models';

import {IExternalDocumentState} from './models';
import * as actionTypes from './actionTypes';


const initialState = {
    isOpen: false,
    fetching: false
} as IExternalDocumentState;


export const reducer = (state = initialState, action: IAction): IExternalDocumentState => {
    switch (action.type) {

        case actionTypes.OPEN:
            return {
                ...state,
                isOpen: true,
                documentID: action.payload.documentID
            };

        case actionTypes.CLOSE:
            return {
                ...state,
                isOpen: false
            };

        case actionTypes.REQUEST.BEGIN: {
            if (state.documentID === action.payload.documentID) {
                return {
                    ...state,
                    fetching: true
                };
            } else {
                return state;
            }
        }

        case actionTypes.REQUEST.END: {
            if (state.documentID === action.payload.documentID) {
                return {
                    ...state,
                    fetching: false
                };
            } else {
                return state;
            }
        }

        case actionTypes.REQUEST.FAILURE: {
            if (state.documentID === action.payload.documentID) {
                return {
                    ...state,
                    fetching: false,
                    error: action.payload.error
                };
            } else {
                return state;
            }
        }

        default:
            return state;

    }
};
