import * as actionTypes from './actionTypes';
const initialState = {
    isOpen: false,
    fetching: false
};
export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.OPEN:
            return Object.assign({}, state, { isOpen: true, documentID: action.payload.documentID });
        case actionTypes.CLOSE:
            return Object.assign({}, state, { isOpen: false });
        case actionTypes.REQUEST.BEGIN: {
            if (state.documentID === action.payload.documentID) {
                return Object.assign({}, state, { fetching: true });
            }
            else {
                return state;
            }
        }
        case actionTypes.REQUEST.END: {
            if (state.documentID === action.payload.documentID) {
                return Object.assign({}, state, { fetching: false });
            }
            else {
                return state;
            }
        }
        case actionTypes.REQUEST.FAILURE: {
            if (state.documentID === action.payload.documentID) {
                return Object.assign({}, state, { fetching: false, error: action.payload.error });
            }
            else {
                return state;
            }
        }
        default:
            return state;
    }
};
//# sourceMappingURL=reducer.js.map