export interface IExternalDocumentState {
    isOpen?: boolean
    fetching?: boolean
    error?: Error
    documentID?: string
}
