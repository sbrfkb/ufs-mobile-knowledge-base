import { StyleSheet } from 'react-native';
import { colors } from '../../../common/constants';
export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    },
    navBar: {
        paddingTop: 20,
        paddingHorizontal: 20,
        height: 64,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: colors.border,
        alignItems: 'center',
        backgroundColor: colors.grayBG
    },
    closeButton: {
        width: 60,
        color: colors.blue
    },
    navBarTitleContainer: {
        flex: 1,
        marginRight: 60
    }
});
//# sourceMappingURL=styles.js.map