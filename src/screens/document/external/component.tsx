import React, {Component} from 'react';
import {ActivityIndicator, StatusBar, TouchableOpacity} from 'react-native';
import {View, FileViewer, Center, H1, Text} from 'ufs-mobile-platform';
import {ExternalDocument} from '../../../data/documents/models';
import styles from './styles';


export interface IProps {
    fetching?: boolean;
    document?: ExternalDocument;

    onCloseButtonPressed?: () => void;
}


export default class ExternalDocumentComponent extends Component<IProps, any> {

    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle={'default'}/>
                <View style={styles.navBar}>
                    <TouchableOpacity onPress={this.props.onCloseButtonPressed}>
                        <Text style={styles.closeButton}>{'Закрыть'}</Text>
                    </TouchableOpacity>
                    <View style={styles.navBarTitleContainer}>
                        <Center>
                            <H1>{this.props.document.title}</H1>
                        </Center>
                    </View>
                </View>
                {this._renderContent()}
            </View>
        );
    }

    private _renderContent = () => {
        if (this.props.fetching) {
            return this._renderActivityIndicator();
        } else {
            return this._renderFileViewer();
        }
    };

    private _renderActivityIndicator = () => {
        return (
            <ActivityIndicator
                animating={true}
                size={'large'}
                style={styles.indicator}
            />
        );
    };

    private _renderFileViewer = () => {
        return (
            <FileViewer
                fileId={this.props.document.fileID}
                style={{ flex: 1, width: 1024 }}
            />
        );
    }

}
