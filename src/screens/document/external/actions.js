import * as actionTypes from './actionTypes';
import { updateDocument } from '../../../data/documents/index';
import api from './api';
export const openExternalDocumentScreen = (document) => {
    const openScreen = (document) => ({
        type: actionTypes.OPEN,
        payload: {
            documentID: document.id
        }
    });
    const request = {
        begin: (document) => ({
            type: actionTypes.REQUEST.BEGIN,
            payload: {
                documentID: document.id
            }
        }),
        end: (document) => ({
            type: actionTypes.REQUEST.END,
            payload: {
                documentID: document.id
            }
        }),
        failure: (document, error) => ({
            type: actionTypes.REQUEST.FAILURE,
            payload: {
                documentID: document.id,
                error
            }
        }),
    };
    return (dispatch) => {
        dispatch(openScreen(document));
        dispatch(request.begin(document));
        if (document.fileID === undefined) {
            api.downloadExternalDocument(document)
                .then((fileID) => {
                document.fileID = fileID;
                dispatch(updateDocument(document));
                dispatch(request.end(document));
            })
                .catch((error) => {
                dispatch(request.failure(document, error));
            });
        }
        else {
            dispatch(request.end(document));
        }
    };
};
export const closeExternalDocumentScreen = () => ({
    type: actionTypes.CLOSE
});
//# sourceMappingURL=actions.js.map