import {reducer} from '../reducer';
import * as actionTypes from '../actionTypes';


describe('externalDocument:reducer', () => {

    it('должен возвращать начальный стейт', () => {
        const action = {
            type: 'test action'
        };
        const initialState = {
            isOpen: false,
            fetching: false
        };

        expect(reducer(initialState, action)).toEqual(initialState);
    });

    it('OPEN', () => {
        const initialState = {
            isOpen: false,
            fetching: false
        };
        const action = {
            type: actionTypes.OPEN,
            payload: {
                documentID: '1'
            }
        };
        const expectedState = {
            isOpen: true,
            fetching: false,
            documentID: '1'
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('CLOSE', () => {
        const initialState = {
            isOpen: true,
            fetching: false
        };
        const action = {
            type: actionTypes.CLOSE
        };
        const expectedState = {
            isOpen: false,
            fetching: false
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    describe('REQUEST', () => {

        describe('правильный ID документа', () => {

            it('BEGIN', () => {
                const initialState = {
                    isOpen: true,
                    fetching: false,
                    documentID: '1'
                };
                const action = {
                    type: actionTypes.REQUEST.BEGIN,
                    payload: {
                        documentID: '1'
                    }
                };
                const expectedState = {
                    isOpen: true,
                    fetching: true,
                    documentID: '1'
                };

                expect(reducer(initialState, action)).toEqual(expectedState);
            });

            it('END', () => {
                const initialState = {
                    isOpen: true,
                    fetching: true,
                    documentID: '1'
                };
                const action = {
                    type: actionTypes.REQUEST.END,
                    payload: {
                        documentID: '1'
                    }
                };
                const expectedState = {
                    isOpen: true,
                    fetching: false,
                    documentID: '1'
                };

                expect(reducer(initialState, action)).toEqual(expectedState);
            });

            it('FAILURE', () => {
                const initialState = {
                    isOpen: true,
                    fetching: false,
                    documentID: '1'
                };
                const error = new Error('error load content');
                const action = {
                    type: actionTypes.REQUEST.FAILURE,
                    payload: {
                        error,
                        documentID: '1'
                    }
                };
                const expectedState = {
                    isOpen: true,
                    fetching: false,
                    documentID: '1',
                    error
                };

                expect(reducer(initialState, action)).toEqual(expectedState);
            });
        });

        describe('неправильный ID документа', () => {

            it('BEGIN', () => {
                const initialState = {
                    isOpen: true,
                    fetching: false,
                    documentID: '1'
                };
                const action = {
                    type: actionTypes.REQUEST.BEGIN,
                    payload: {
                        documentID: '2'
                    }
                };

                expect(reducer(initialState, action)).toEqual(initialState);
            });

            it('END', () => {
                const initialState = {
                    isOpen: true,
                    fetching: true,
                    documentID: '1'
                };
                const action = {
                    type: actionTypes.REQUEST.END,
                    payload: {
                        documentID: '2'
                    }
                };

                expect(reducer(initialState, action)).toEqual(initialState);
            });

            it('FAILURE', () => {
                const initialState = {
                    isOpen: true,
                    fetching: true,
                    documentID: '1'
                };
                const error = new Error('error load content');
                const action = {
                    type: actionTypes.REQUEST.FAILURE,
                    payload: {
                        error,
                        documentID: '2'
                    }
                };

                expect(reducer(initialState, action)).toEqual(initialState);
            });
        });

    });

});
