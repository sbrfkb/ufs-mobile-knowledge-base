import React from 'react';
import {StyleSheet} from 'react-native';
import {colors} from '../../../common/constants';


export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white'
    } as React.ViewStyle,

    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    } as React.ViewStyle,

    navBar: {
        paddingTop: 20,
        paddingHorizontal: 20,
        height: 64,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: colors.border,
        alignItems: 'center',
        backgroundColor: colors.grayBG
    } as React.ViewStyle,

    closeButton: {
        width: 60,
        color: colors.blue
    } as React.TextStyle,

    navBarTitleContainer: {
        flex: 1,
        marginRight: 60
    } as React.ViewStyle

});