import { connect } from 'react-redux';
import Component from './component';
import { closeExternalDocumentScreen } from './actions';
const mapStateToProps = (state) => {
    const externalDocumentState = state.user.knowledgeBase.screens.externalDocument;
    const documentsState = state.user.knowledgeBase.data.documents;
    return {
        fetching: externalDocumentState.fetching,
        document: documentsState.itemsByID.get(externalDocumentState.documentID)
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        onCloseButtonPressed: () => {
            dispatch(closeExternalDocumentScreen());
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Component);
//# sourceMappingURL=index.js.map