import {connect} from 'react-redux';

import {IGlobalState} from '../../../common/models';
import {ExternalDocument} from '../../../data/documents/models';

import Component, {IProps} from './component';
import {closeExternalDocumentScreen} from './actions';


const mapStateToProps = (state: IGlobalState): IProps => {
    const externalDocumentState = state.user.knowledgeBase.screens.externalDocument;
    const documentsState = state.user.knowledgeBase.data.documents;

    return {
        fetching: externalDocumentState.fetching,
        document: documentsState.itemsByID.get(externalDocumentState.documentID) as ExternalDocument
    };
};


const mapDispatchToProps = (dispatch): IProps => {
    return {
        onCloseButtonPressed: () => {
            dispatch(closeExternalDocumentScreen());
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Component);
