import {apiSource} from '../../../../common/constants';

import ecm from './ecm';
import fileShare from './fileShare';


const api = apiSource === 'ecm' ? ecm : fileShare;

export default api;
