import {ExternalDocument} from '../../../../data/documents/models';
import {getTFS, postTFS, download} from '../../../../services/api/ecm/index';

import {IExternalDocumentsAPI} from './models';
import ECMAPIConfig from '../../../../services/api/ecm/config';


const tfsLimits = {
    getStatusInterval: 4,
    getStatusPeriod: 60
};


class ExternalDocumentECMAPI implements IExternalDocumentsAPI {

    downloadExternalDocument(document: ExternalDocument): Promise<string> {
        return this.getTFSFile(document.id)
            .catch(e => {
                console.log(e);
            });
    }

    private delay = (ms: number) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    };

    private async getTFSFile(fileID: string) {
        const checkStatus = async (operationID: string) => {
            return await getTFS(`/file/status/${operationID}`);
        };

        const moveFile = async (): Promise<any> => {
            const requestParams = {
                fileId: fileID,
                scenarioId: ECMAPIConfig.sharedInstance.scenarioID,
                cachePolicy: {
                    type: 'DIRECT',
                    maxTTL: 5
                }
            };

            return await postTFS(`/file/download`, requestParams);
        };

        const downloadFile = async (filename: string): Promise<string> => {
            return await download(`/file/${filename}`);
        };

        let status = null;
        const getFilePath = async () => {
            const moveResponse = await moveFile();
            const startTime = new Date();

            if (moveResponse.success && moveResponse.body.operationId) {
                while (status !== 'SUCCEEDED') {
                    await this.delay(tfsLimits.getStatusInterval * 1000);
                    let moveStatus = await checkStatus(moveResponse.body.operationId);

                    if (moveStatus && moveStatus.statusCode) {
                        status = moveStatus.statusCode;
                    }

                    switch (status) {
                        case 'SUCCEEDED':
                            return await downloadFile(moveStatus.file.fileInfo.name);

                        case 'EXPIRED':
                            return await getFilePath();

                        case 'IN_PROGRESS':
                            const duration = Math.abs(new Date().getTime() - startTime.getTime());
                            if (duration > tfsLimits.getStatusPeriod * 1000) {
                                throw new Error(`TFS ${fileID} - превышено время ожидания от перекладчика`);
                            }
                            break;

                        case 'FAILED_TFS':
                            throw new Error(`TFS ${fileID} - ${status}`);

                        case 'FAILED_ECM':
                            throw new Error(`ECM ${fileID} - ${status}`);
                    }
                }
                return null;
            } else if (moveResponse.body.status && moveResponse.body.status.statusCode == 'SUCCEEDED') {
                status = moveResponse.body.status.statusCode;
                return await downloadFile(moveResponse.body.status.file.fileInfo.name);
            } else {
                return null;
            }
        };

        return await getFilePath();
    }

}

const api = new ExternalDocumentECMAPI();

export default api;
