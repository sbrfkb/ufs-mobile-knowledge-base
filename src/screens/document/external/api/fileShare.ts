import {ExternalDocument} from '../../../../data/documents/models';
import {download} from '../../../../services/api/fileShare/index';

import {IExternalDocumentsAPI} from './models';


class ExternalDocumentFileShareAPI implements IExternalDocumentsAPI {

    downloadExternalDocument(document: ExternalDocument): Promise<string> {
        return download('/content', true, {path: document.path});
    }

}

const api = new ExternalDocumentFileShareAPI();

export default api;
