var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { getTFS, postTFS, download } from '../../../../services/api/ecm/index';
import ECMAPIConfig from '../../../../services/api/ecm/config';
const tfsLimits = {
    getStatusInterval: 4,
    getStatusPeriod: 60
};
class ExternalDocumentECMAPI {
    constructor() {
        this.delay = (ms) => {
            return new Promise(resolve => setTimeout(resolve, ms));
        };
    }
    downloadExternalDocument(document) {
        return this.getTFSFile(document.id)
            .catch(e => {
            console.log(e);
        });
    }
    getTFSFile(fileID) {
        return __awaiter(this, void 0, void 0, function* () {
            const checkStatus = (operationID) => __awaiter(this, void 0, void 0, function* () {
                return yield getTFS(`/file/status/${operationID}`);
            });
            const moveFile = () => __awaiter(this, void 0, void 0, function* () {
                const requestParams = {
                    fileId: fileID,
                    scenarioId: ECMAPIConfig.sharedInstance.scenarioID,
                    cachePolicy: {
                        type: 'DIRECT',
                        maxTTL: 5
                    }
                };
                return yield postTFS(`/file/download`, requestParams);
            });
            const downloadFile = (filename) => __awaiter(this, void 0, void 0, function* () {
                return yield download(`/file/${filename}`);
            });
            let status = null;
            const getFilePath = () => __awaiter(this, void 0, void 0, function* () {
                const moveResponse = yield moveFile();
                const startTime = new Date();
                if (moveResponse.success && moveResponse.body.operationId) {
                    while (status !== 'SUCCEEDED') {
                        yield this.delay(tfsLimits.getStatusInterval * 1000);
                        let moveStatus = yield checkStatus(moveResponse.body.operationId);
                        if (moveStatus && moveStatus.statusCode) {
                            status = moveStatus.statusCode;
                        }
                        switch (status) {
                            case 'SUCCEEDED':
                                return yield downloadFile(moveStatus.file.fileInfo.name);
                            case 'EXPIRED':
                                return yield getFilePath();
                            case 'IN_PROGRESS':
                                const duration = Math.abs(new Date().getTime() - startTime.getTime());
                                if (duration > tfsLimits.getStatusPeriod * 1000) {
                                    throw new Error(`TFS ${fileID} - превышено время ожидания от перекладчика`);
                                }
                                break;
                            case 'FAILED_TFS':
                                throw new Error(`TFS ${fileID} - ${status}`);
                            case 'FAILED_ECM':
                                throw new Error(`ECM ${fileID} - ${status}`);
                        }
                    }
                    return null;
                }
                else if (moveResponse.body.status && moveResponse.body.status.statusCode == 'SUCCEEDED') {
                    status = moveResponse.body.status.statusCode;
                    return yield downloadFile(moveResponse.body.status.file.fileInfo.name);
                }
                else {
                    return null;
                }
            });
            return yield getFilePath();
        });
    }
}
const api = new ExternalDocumentECMAPI();
export default api;
//# sourceMappingURL=ecm.js.map