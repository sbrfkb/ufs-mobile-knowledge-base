import { download } from '../../../../services/api/fileShare/index';
class ExternalDocumentFileShareAPI {
    downloadExternalDocument(document) {
        return download('/content', true, { path: document.path });
    }
}
const api = new ExternalDocumentFileShareAPI();
export default api;
//# sourceMappingURL=fileShare.js.map