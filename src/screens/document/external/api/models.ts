import {ExternalDocument} from '../../../../data/documents/models';


export interface IExternalDocumentsAPI {
    downloadExternalDocument(document: ExternalDocument): Promise<string>
}
