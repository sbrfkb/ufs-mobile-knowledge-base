import React, { Component } from 'react';
import { ActivityIndicator, StatusBar, TouchableOpacity } from 'react-native';
import { View, FileViewer, Center, H1, Text } from 'ufs-mobile-platform';
import styles from './styles';
export default class ExternalDocumentComponent extends Component {
    constructor() {
        super(...arguments);
        this._renderContent = () => {
            if (this.props.fetching) {
                return this._renderActivityIndicator();
            }
            else {
                return this._renderFileViewer();
            }
        };
        this._renderActivityIndicator = () => {
            return (React.createElement(ActivityIndicator, { animating: true, size: 'large', style: styles.indicator }));
        };
        this._renderFileViewer = () => {
            return (React.createElement(FileViewer, { fileId: this.props.document.fileID, style: { flex: 1, width: 1024 } }));
        };
    }
    render() {
        return (React.createElement(View, { style: styles.container },
            React.createElement(StatusBar, { barStyle: 'default' }),
            React.createElement(View, { style: styles.navBar },
                React.createElement(TouchableOpacity, { onPress: this.props.onCloseButtonPressed },
                    React.createElement(Text, { style: styles.closeButton }, 'Закрыть')),
                React.createElement(View, { style: styles.navBarTitleContainer },
                    React.createElement(Center, null,
                        React.createElement(H1, null, this.props.document.title)))),
            this._renderContent()));
    }
}
//# sourceMappingURL=component.js.map