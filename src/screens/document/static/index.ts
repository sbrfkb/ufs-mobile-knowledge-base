import {connect} from 'react-redux';

import {IGlobalState} from '../../../common/models';
import {StaticDocument, StaticDocumentSection} from '../../../data/documents/models';

import Component, {IProps} from './component';
import {childs} from '../../../data/tree/selectors';


const mapStateToProps = (state: IGlobalState): IProps => {
    const documentsState = state.user.knowledgeBase.data.documents;

    if (documentsState.fetching) {
        return {
            fetching: true
        };
    } else {
        const treeState = state.user.knowledgeBase.data.tree;
        const selectedTreeItem = documentsState.itemsByID.get(treeState.selectedID);

        if (!(selectedTreeItem instanceof StaticDocument)) {
            return {
                fetching: false
            }
        }

        const document = selectedTreeItem as StaticDocument;

        let sections: StaticDocumentSection[] = [];

        const childDocuments = childs(documentsState.itemsByID, document.id);
        if (childDocuments.length > 0) {
            const documentsSection = new StaticDocumentSection();
            documentsSection.title = 'Документы';
            documentsSection.template = 'documents';
            documentsSection.data = childDocuments;
            sections.push(documentsSection);
        }

        document.sections = sections;

        return {
            fetching: false,
            document
        };
    }
};


const mapDispatchToProps = (): IProps => {
    return {
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Component);
