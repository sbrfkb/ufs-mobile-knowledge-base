import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native';
import { Accordion, Section, Text, View } from 'ufs-mobile-platform';
import sectionsFactory from './templates/factory';
import styles from './styles';
export default class StaticDocumentComponent extends Component {
    constructor(props) {
        super(props);
        this._renderPageInfo = () => {
            return (React.createElement(View, null,
                React.createElement(View, { style: styles.container },
                    React.createElement(Text, { style: styles.sectionTitle }, 'О продукте'),
                    React.createElement(Text, { style: styles.aboutProduct }, this.props.document.content)),
                this._renderSections()));
        };
        this._renderSections = () => {
            return this.props.document.sections.map((section, index) => {
                return (React.createElement(Section, { key: `section_${index}` },
                    React.createElement(Accordion, { header: section.title, opened: this.state.openedSections.has(index), onClick: () => this._toggleSection(index) }, sectionsFactory(section))));
            });
        };
        this._toggleSection = (index) => {
            const openedSections = this.state.openedSections;
            if (openedSections.has(index)) {
                openedSections.delete(index);
            }
            else {
                openedSections.add(index);
            }
            this.setState({ openedSections });
        };
        this.state = {
            openedSections: new Set()
        };
    }
    render() {
        if (this.props.fetching) {
            return this._renderActivityIndicator();
        }
        else if (this.props.document != null) {
            return this._renderPageInfo();
        }
        else {
            return null;
        }
    }
    _renderActivityIndicator() {
        return (React.createElement(View, { style: [styles.container, { height: 300 }] },
            React.createElement(ActivityIndicator, { animating: true, size: 'large', style: styles.indicator })));
    }
}
//# sourceMappingURL=component.js.map