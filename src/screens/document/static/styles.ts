import React from 'react';
import {StyleSheet} from 'react-native';
import {colors, fonts} from '../../../common/constants';


export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 10
    } as React.ViewStyle,

    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    } as React.ViewStyle,

    sectionTitle: {
        marginTop: 10,
        fontFamily: fonts.bold,
        fontSize: 16,
        color: colors.black
    } as React.TextStyle,

    aboutProduct: {
        marginVertical: 10,
        fontFamily: fonts.regular,
        fontSize: 16,
        color: colors.black
    } as React.TextStyle

});