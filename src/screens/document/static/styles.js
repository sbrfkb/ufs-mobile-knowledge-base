import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../common/constants';
export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    },
    sectionTitle: {
        marginTop: 10,
        fontFamily: fonts.bold,
        fontSize: 16,
        color: colors.black
    },
    aboutProduct: {
        marginVertical: 10,
        fontFamily: fonts.regular,
        fontSize: 16,
        color: colors.black
    }
});
//# sourceMappingURL=styles.js.map