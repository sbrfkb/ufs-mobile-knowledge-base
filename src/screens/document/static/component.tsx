import React, {Component} from 'react';
import {ActivityIndicator} from 'react-native';
import {Accordion, Section, Text, View} from 'ufs-mobile-platform';
import {StaticDocument, StaticDocumentSection} from '../../../data/documents/models';

import sectionsFactory from './templates/factory';
import styles from './styles';


export interface IProps {
    fetching?: boolean
    document?: StaticDocument
}


interface IState {
    openedSections?: Set<number>
}


export default class StaticDocumentComponent extends Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);

        this.state = {
            openedSections: new Set()
        };
    }

    render() {
        if (this.props.fetching) {
            return this._renderActivityIndicator();
        } else if (this.props.document != null) {
            return this._renderPageInfo();
        } else {
            return null;
        }
    }

    private _renderActivityIndicator() {
        return (
            <View style={[styles.container, {height: 300}]}>
                <ActivityIndicator
                    animating={true}
                    size={'large'}
                    style={styles.indicator}
                />
            </View>
        );
    }

    private _renderPageInfo = () => {
        return (
            <View>
                <View style={styles.container}>
                    <Text style={styles.sectionTitle}>{'О продукте'}</Text>
                    <Text style={styles.aboutProduct}>{this.props.document.content}</Text>
                </View>

                {this._renderSections()}
            </View>
        );
    };

    private _renderSections = () => {
        return this.props.document.sections.map((section: StaticDocumentSection, index) => {
            return (
                <Section key={`section_${index}`}>
                    <Accordion header={section.title} opened={this.state.openedSections.has(index)} onClick={() => this._toggleSection(index)}>
                        {sectionsFactory(section)}
                    </Accordion>
                </Section>
            );
        });
    };

    private _toggleSection = (index) => {
        const openedSections = this.state.openedSections;
        if (openedSections.has(index)) {
            openedSections.delete(index);
        } else {
            openedSections.add(index);
        }
        this.setState({openedSections});
    };

}
