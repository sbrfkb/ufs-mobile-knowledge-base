import React, { Component } from 'react';
import { TouchableHighlight, ListView } from 'react-native';
import { Text, View } from 'ufs-mobile-platform';
import { ExternalDocument } from '../../../../../data/documents/models';
import styles from './styles';
import { colors } from '../../../../../common/constants';
export default class DocumentsSectionTemplate extends Component {
    constructor(props) {
        super(props);
        this._renderRow = (document) => {
            return (React.createElement(TouchableHighlight, { underlayColor: colors.highlight, onPress: () => this.props.showDocument(document) },
                React.createElement(View, { style: styles.container },
                    React.createElement(View, { style: [styles.rowTypeContainer, { backgroundColor: document instanceof ExternalDocument ? colors.orange : colors.green }] },
                        React.createElement(Text, { style: styles.rowTypeTitle }, document instanceof ExternalDocument ? 'PDF' : 'READ')),
                    React.createElement(Text, { style: styles.rowTitle, numberOfLines: 1 }, document.title))));
        };
        this._renderSeparator = (sectionID, rowID) => {
            return (React.createElement(View, { key: `${sectionID}-${rowID}`, style: styles.separator }));
        };
        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            })
        };
    }
    render() {
        return (React.createElement(ListView, { dataSource: this.state.dataSource.cloneWithRows(this.props.section.data), renderRow: this._renderRow, renderSeparator: this._renderSeparator }));
    }
}
//# sourceMappingURL=component.js.map