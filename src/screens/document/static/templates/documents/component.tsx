import React, {Component} from 'react';
import {TouchableHighlight, ListView, ListViewDataSource} from 'react-native';
import {Text, View} from 'ufs-mobile-platform';

import {Document, ExternalDocument, StaticDocumentSection} from '../../../../../data/documents/models';

import styles from './styles';
import {colors} from '../../../../../common/constants';


export interface IProps {
    section?: StaticDocumentSection

    showDocument?: (document: ExternalDocument) => void
}

interface IState {
    dataSource: ListViewDataSource
}


export default class DocumentsSectionTemplate extends Component<IProps, IState> {

    constructor(props) {
        super(props);

        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            })
        };
    }

    render() {
        return (
            <ListView
                dataSource={this.state.dataSource.cloneWithRows(this.props.section.data)}
                renderRow={this._renderRow}
                renderSeparator={this._renderSeparator}
            />
        );
    }

    private _renderRow = (document: Document) => {
        return (
            <TouchableHighlight underlayColor={colors.highlight} onPress={() => this.props.showDocument(document)}>
                <View style={styles.container}>
                    <View
                        style={[styles.rowTypeContainer, {backgroundColor: document instanceof ExternalDocument ? colors.orange : colors.green}]}>
                        <Text style={styles.rowTypeTitle}>
                            {document instanceof ExternalDocument ? 'PDF' : 'READ'}
                        </Text>
                    </View>
                    <Text style={styles.rowTitle} numberOfLines={1}>
                        {document.title}
                    </Text>
                </View>
            </TouchableHighlight>
        );
    };

    private _renderSeparator = (sectionID: React.ReactText, rowID: React.ReactText) => {
        return (
            <View
                key={`${sectionID}-${rowID}`}
                style={styles.separator}
            />
        );
    };

}
