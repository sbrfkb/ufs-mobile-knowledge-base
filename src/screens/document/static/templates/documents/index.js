import { connect } from 'react-redux';
import Component from './component';
import { openExternalDocumentScreen } from '../../../external/actions';
const mapStateToProps = () => {
    return {};
};
const mapDispatchToProps = (dispatch) => {
    return {
        showDocument: (document) => {
            dispatch(openExternalDocumentScreen(document));
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Component);
//# sourceMappingURL=index.js.map