import React from 'react';
import {StyleSheet} from 'react-native';

import {colors, fonts} from '../../../../../common/constants';


export default StyleSheet.create({

    container: {
        marginLeft: 20,
        flexDirection: 'row',
        alignItems: 'center',
        height: 40
    } as React.ViewStyle,

    rowTypeContainer: {
        paddingHorizontal: 3,
        paddingVertical: 1,
        borderRadius: 3,
        height: 14
    } as React.ViewStyle,

    rowTypeTitle: {
        fontFamily: fonts.bold,
        fontSize: 8,
        color: 'white'
    } as React.TextStyle,

    rowTitle: {
        marginLeft: 10,
        fontFamily: fonts.regular,
        fontSize: 16
    } as React.TextStyle,

    separator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: colors.border,
        marginLeft: 20
    } as React.ViewStyle

});