import {connect} from 'react-redux';

import Component, {IProps} from './component';
import {openExternalDocumentScreen} from '../../../external/actions';


const mapStateToProps = (): IProps => {
    return {
    };
};


const mapDispatchToProps = (dispatch): IProps => {
    return {
        showDocument: (document) => {
            dispatch(openExternalDocumentScreen(document));
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Component);
