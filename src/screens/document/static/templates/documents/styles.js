import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../../../common/constants';
export default StyleSheet.create({
    container: {
        marginLeft: 20,
        flexDirection: 'row',
        alignItems: 'center',
        height: 40
    },
    rowTypeContainer: {
        paddingHorizontal: 3,
        paddingVertical: 1,
        borderRadius: 3,
        height: 14
    },
    rowTypeTitle: {
        fontFamily: fonts.bold,
        fontSize: 8,
        color: 'white'
    },
    rowTitle: {
        marginLeft: 10,
        fontFamily: fonts.regular,
        fontSize: 16
    },
    separator: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: colors.border,
        marginLeft: 20
    }
});
//# sourceMappingURL=styles.js.map