import React, { Component } from 'react';
import { Text, View } from 'ufs-mobile-platform';
import styles from './styles';
export default class TextSectionTemplate extends Component {
    render() {
        return (React.createElement(View, { style: styles.container },
            React.createElement(Text, { style: styles.text }, this.props.section.data)));
    }
}
//# sourceMappingURL=index.js.map