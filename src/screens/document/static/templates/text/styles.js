import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../../../common/constants';
export default StyleSheet.create({
    container: {
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    text: {
        fontFamily: fonts.regular,
        fontSize: 16,
        color: colors.black,
        backgroundColor: 'transparent'
    }
});
//# sourceMappingURL=styles.js.map