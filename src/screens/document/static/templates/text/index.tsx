import React, {Component} from 'react';
import {Text, View} from 'ufs-mobile-platform';

import {StaticDocumentSection} from '../../../../../data/documents/models';

import styles from './styles';


export interface IProps {
    section?: StaticDocumentSection;
}


export default class TextSectionTemplate extends Component<IProps, any> {

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>{this.props.section.data}</Text>
            </View>
        );
    }

}
