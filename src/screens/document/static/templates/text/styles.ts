import React from 'react';
import {StyleSheet} from 'react-native';

import {colors, fonts} from '../../../../../common/constants';


export default StyleSheet.create({

    container: {
        paddingVertical: 10,
        paddingHorizontal: 20
    } as React.ViewStyle,

    text: {
        fontFamily: fonts.regular,
        fontSize: 16,
        color: colors.black,
        backgroundColor: 'transparent'
    } as React.TextStyle

});