import React from 'react';

import {StaticDocumentSection} from '../../../../data/documents/models';

import TextSectionTemplate from './text/index';
import DocumentsSectionTemplate from './documents/index';


export default (section: StaticDocumentSection) => {
    switch (section.template) {
        case 'text':
            return <TextSectionTemplate section={section}/>;

        case 'documents':
            return <DocumentsSectionTemplate section={section}/>;

        default:
            return null;
    }
};
