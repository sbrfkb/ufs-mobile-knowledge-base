import React from 'react';
import TextSectionTemplate from './text/index';
import DocumentsSectionTemplate from './documents/index';
export default (section) => {
    switch (section.template) {
        case 'text':
            return React.createElement(TextSectionTemplate, { section: section });
        case 'documents':
            return React.createElement(DocumentsSectionTemplate, { section: section });
        default:
            return null;
    }
};
//# sourceMappingURL=factory.js.map