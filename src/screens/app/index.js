import { connect } from 'react-redux';
import { loadTree } from '../../data/tree/index';
import { filterGroups } from '../../data/tree/selectors';
import AppComponent from './component';
const mapStateToProps = (state) => {
    const treeState = state.user.knowledgeBase.data.tree;
    if (treeState.fetching) {
        return {
            fetching: true
        };
    }
    else {
        const documentsState = state.user.knowledgeBase.data.documents;
        return {
            fetching: false,
            groups: filterGroups(documentsState.itemsByID)
        };
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        onComponentDidMount: () => {
            dispatch(loadTree());
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(AppComponent);
//# sourceMappingURL=index.js.map