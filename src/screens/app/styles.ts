import React from 'react';
import {StyleSheet} from 'react-native';


export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white'
    } as React.ViewStyle,

    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    } as React.ViewStyle

});