import React, { Component } from 'react';
import { AccessoryPanel, CenterPageHeader, ContentPanel, H2, Page, SplitPanel, View } from 'ufs-mobile-platform';
import { ActivityIndicator } from 'react-native';
import ContentScreen from '../content';
import MenuScreen from '../menu';
import styles from './styles';
export default class AppComponent extends Component {
    constructor() {
        super(...arguments);
        this._renderActivityIndicator = () => {
            return (React.createElement(View, { style: styles.container },
                React.createElement(ActivityIndicator, { animating: true, size: 'large', style: styles.indicator })));
        };
        this._renderMainContent = () => {
            return (React.createElement(SplitPanel, { id: 'knowledgeBasePanel' },
                React.createElement(AccessoryPanel, null, this._renderAccessoryPanels()),
                React.createElement(ContentPanel, { style: { backgroundColor: 'white' } },
                    React.createElement(ContentScreen, null))));
        };
        this._renderAccessoryPanels = () => {
            return this.props.groups.map((group) => {
                return (React.createElement(Page, { key: group.id, content: React.createElement(MenuScreen, { groupID: group.id }) },
                    React.createElement(CenterPageHeader, null,
                        React.createElement(H2, null, group.title))));
            });
        };
    }
    componentDidMount() {
        this.props.onComponentDidMount();
    }
    render() {
        if (this.props.fetching) {
            return this._renderActivityIndicator();
        }
        else {
            return this._renderMainContent();
        }
    }
}
//# sourceMappingURL=component.js.map