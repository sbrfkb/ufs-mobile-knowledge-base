import React, {Component} from 'react';
import {AccessoryPanel, CenterPageHeader, ContentPanel, H2, Page, SplitPanel, View} from 'ufs-mobile-platform';
import {ActivityIndicator} from 'react-native';
import ContentScreen from '../content';
import MenuScreen from '../menu';
import {Group} from '../../data/tree/models';

import styles from './styles';


export interface IProps {
    fetching?: boolean
    groups?: Group[]

    onComponentDidMount?: () => void
}


export default class AppComponent extends Component<IProps, any> {

    componentDidMount() {
        this.props.onComponentDidMount();
    }

    render() {
        if (this.props.fetching) {
            return this._renderActivityIndicator();
        } else {
            return this._renderMainContent();
        }
    }

    private _renderActivityIndicator = () => {
        return (
            <View style={styles.container}>
                <ActivityIndicator
                    animating={true}
                    size={'large'}
                    style={styles.indicator}
                />
            </View>
        );
    };

    private _renderMainContent = () => {
        return (
            <SplitPanel id='knowledgeBasePanel'>
                <AccessoryPanel>
                    {this._renderAccessoryPanels()}
                </AccessoryPanel>
                <ContentPanel style={{backgroundColor: 'white'}}>
                    <ContentScreen/>
                </ContentPanel>
            </SplitPanel>
        );
    };

    private _renderAccessoryPanels = () => {
        return this.props.groups.map((group) => {
            return (
                <Page key={group.id} content={<MenuScreen groupID={group.id}/>}>
                    <CenterPageHeader>
                        <H2>{group.title}</H2>
                    </CenterPageHeader>
                </Page>
            );
        });
    };
}
