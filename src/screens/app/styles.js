import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    }
});
//# sourceMappingURL=styles.js.map