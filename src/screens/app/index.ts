import {connect} from 'react-redux';

import {IGlobalState} from '../../common/models';
import {loadTree} from '../../data/tree/index';
import {filterGroups} from '../../data/tree/selectors';

import AppComponent, {IProps} from './component';


const mapStateToProps = (state: IGlobalState): IProps => {
    const treeState = state.user.knowledgeBase.data.tree;

    if (treeState.fetching) {
        return {
            fetching: true
        };
    } else {
        const documentsState = state.user.knowledgeBase.data.documents;

        return {
            fetching: false,
            groups: filterGroups(documentsState.itemsByID)
        };
    }
};


const mapDispatchToProps = (dispatch): IProps => {
    return {
        onComponentDidMount: () => {
            dispatch(loadTree());
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(AppComponent);
