import React, {Component} from 'react';
import {Text, View} from 'ufs-mobile-platform';
import {Image, ListView, SegmentedControlIOS, TouchableHighlight} from 'react-native';

import {Group, ITreeItem} from '../../data/tree/models';
import {Document} from '../../data/documents/models';

import styles from './styles';


export interface IProps {
    groupID?: string

    group?: Group
    sections?: ITreeItem[]
    favorites?: Document[]
    selectedTreeItemID?: string

    selectTreeItem?: (treeItem: ITreeItem) => void
}


interface IState {
    segmentIndex: number
    ds: React.ListViewDataSource
}


export default class MenuComponent extends Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);

        this.state = {
            segmentIndex: 0,
            ds: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            })
        } as IState;
    }

    render() {
        let items = this.state.segmentIndex == 0 ? this.props.sections : [this.props.favorites];
        return (
            <View style={styles.container}>
                <ListView
                    dataSource={this.state.ds.cloneWithRows(items)}
                    renderRow={this._renderRow}
                    renderSeparator={this._renderSeparator}
                    enableEmptySections={true}
                />
            </View>
        );
    }

    private _renderHeader = () => {
        if (this.props.group.parentID == null) {
            return (
                <View style={{backgroundColor: 'white', padding: 15}}>
                    <SegmentedControlIOS
                        values={['Структура', 'Моя библиотека']}
                        selectedIndex={this.state.segmentIndex}
                        onChange={(event) => {
                            this.setState({segmentIndex: event.nativeEvent.selectedSegmentIndex});
                        }}
                        tintColor={'gray'}
                    />
                </View>
            );
        } else {
            return null;
        }
    };

    private _renderRow = (treeItem: ITreeItem) => {
        const isSelected = treeItem.id == this.props.selectedTreeItemID;

        return (
            <TouchableHighlight onPress={() => {
                this.props.selectTreeItem(treeItem);
            }}>
                <View style={[styles.cellContainer, isSelected ? styles.selectedCellContainer : null]}>
                    <Text style={[styles.text, isSelected ? {color: 'white'} : null]}>{treeItem.title}</Text>
                    {treeItem instanceof Group &&
                    <Image
                        source={require('./images/arrow.png')}
                        style={styles.arrowImage}
                    />
                    }
                </View>
            </TouchableHighlight>
        );
    };

    private _renderSeparator = (sectionID: React.ReactText, rowID: React.ReactText) => {
        return (
            <View key={`${sectionID}-${rowID}`} style={styles.separatorContainer}>
                <View style={styles.separator} />
            </View>
        );
    };

}
