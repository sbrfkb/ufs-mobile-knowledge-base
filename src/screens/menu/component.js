import React, { Component } from 'react';
import { Text, View } from 'ufs-mobile-platform';
import { Image, ListView, SegmentedControlIOS, TouchableHighlight } from 'react-native';
import { Group } from '../../data/tree/models';
import styles from './styles';
export default class MenuComponent extends Component {
    constructor(props) {
        super(props);
        this._renderHeader = () => {
            if (this.props.group.parentID == null) {
                return (React.createElement(View, { style: { backgroundColor: 'white', padding: 15 } },
                    React.createElement(SegmentedControlIOS, { values: ['Структура', 'Моя библиотека'], selectedIndex: this.state.segmentIndex, onChange: (event) => {
                            this.setState({ segmentIndex: event.nativeEvent.selectedSegmentIndex });
                        }, tintColor: 'gray' })));
            }
            else {
                return null;
            }
        };
        this._renderRow = (treeItem) => {
            const isSelected = treeItem.id == this.props.selectedTreeItemID;
            return (React.createElement(TouchableHighlight, { onPress: () => {
                    this.props.selectTreeItem(treeItem);
                } },
                React.createElement(View, { style: [styles.cellContainer, isSelected ? styles.selectedCellContainer : null] },
                    React.createElement(Text, { style: [styles.text, isSelected ? { color: 'white' } : null] }, treeItem.title),
                    treeItem instanceof Group &&
                        React.createElement(Image, { source: require('./images/arrow.png'), style: styles.arrowImage }))));
        };
        this._renderSeparator = (sectionID, rowID) => {
            return (React.createElement(View, { key: `${sectionID}-${rowID}`, style: styles.separatorContainer },
                React.createElement(View, { style: styles.separator })));
        };
        this.state = {
            segmentIndex: 0,
            ds: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            })
        };
    }
    render() {
        let items = this.state.segmentIndex == 0 ? this.props.sections : [this.props.favorites];
        return (React.createElement(View, { style: styles.container },
            React.createElement(ListView, { dataSource: this.state.ds.cloneWithRows(items), renderRow: this._renderRow, renderSeparator: this._renderSeparator, enableEmptySections: true })));
    }
}
//# sourceMappingURL=component.js.map