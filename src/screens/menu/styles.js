import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../common/constants';
export default StyleSheet.create({
    container: {},
    cellContainer: {
        backgroundColor: 'white',
        paddingHorizontal: 20,
        alignItems: 'center',
        paddingVertical: 12,
        flexDirection: 'row'
    },
    separatorContainer: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: 'white',
        paddingLeft: 20
    },
    separator: {
        flex: 1,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: colors.border
    },
    text: {
        flex: 1,
        fontFamily: fonts.regular,
        fontSize: 15
    },
    selectedCellContainer: {
        backgroundColor: colors.highlight
    },
    arrowImage: {
        width: 7,
        height: 12
    }
});
//# sourceMappingURL=styles.js.map