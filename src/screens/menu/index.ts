import {connect} from 'react-redux';

import {IGlobalState} from '../../common/models';

import Component, {IProps} from './component';
import {Group, ITreeItem} from '../../data/tree/models';
import * as tree from '../../data/tree/index';
import {childs} from '../../data/tree/selectors';
import {filterFavorites} from '../../data/documents/selectors';


const mapStateToProps = (state: IGlobalState, ownProps: IProps): IProps => {
    const treeState = state.user.knowledgeBase.data.tree;
    const documentsState = state.user.knowledgeBase.data.documents;

    return {
        selectedTreeItemID: treeState.selectedID,
        group: documentsState.itemsByID.get(ownProps.groupID) as Group,
        sections: childs(documentsState.itemsByID, ownProps.groupID),
        favorites: filterFavorites(documentsState.itemsByID)
    };
};


const mapDispatchToProps = (dispatch): IProps => {
    return {
        selectTreeItem: (item: ITreeItem) => {
            dispatch(tree.select(item));
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Component);
