import { connect } from 'react-redux';
import Component from './component';
import * as tree from '../../data/tree/index';
import { childs } from '../../data/tree/selectors';
import { filterFavorites } from '../../data/documents/selectors';
const mapStateToProps = (state, ownProps) => {
    const treeState = state.user.knowledgeBase.data.tree;
    const documentsState = state.user.knowledgeBase.data.documents;
    return {
        selectedTreeItemID: treeState.selectedID,
        group: documentsState.itemsByID.get(ownProps.groupID),
        sections: childs(documentsState.itemsByID, ownProps.groupID),
        favorites: filterFavorites(documentsState.itemsByID)
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        selectTreeItem: (item) => {
            dispatch(tree.select(item));
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Component);
//# sourceMappingURL=index.js.map