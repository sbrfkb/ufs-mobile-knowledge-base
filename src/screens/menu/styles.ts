import React from 'react';
import {StyleSheet} from 'react-native';
import {colors, fonts} from '../../common/constants';


export default StyleSheet.create({

    container: {

    } as React.ViewStyle,

    cellContainer: {
        backgroundColor: 'white',
        paddingHorizontal: 20,
        alignItems: 'center',
        paddingVertical: 12,
        flexDirection: 'row'
    } as React.ViewStyle,

    separatorContainer: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: 'white',
        paddingLeft: 20
    } as React.ViewStyle,

    separator: {
        flex: 1,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: colors.border
    } as React.ViewStyle,

    text: {
        flex: 1,
        fontFamily: fonts.regular,
        fontSize: 15
    } as React.TextStyle,

    selectedCellContainer: {
        backgroundColor: colors.highlight
    } as React.ViewStyle,

    arrowImage: {
        width: 7,
        height: 12
    } as React.ImageStyle

});
