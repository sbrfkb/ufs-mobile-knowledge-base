import { combineReducers } from 'redux';
import { reducer as searchReducer } from './search/reducer';
import { reducer as externalDocumentReducer } from './document/external/reducer';
export const reducer = combineReducers({
    search: searchReducer,
    externalDocument: externalDocumentReducer
});
//# sourceMappingURL=reducer.js.map