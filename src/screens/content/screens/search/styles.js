import { StyleSheet } from 'react-native';
import { colors, fonts } from '../../../../common/constants';
export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    searchBarContainer: {
        height: 44,
        width: 640,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 12,
        borderBottomColor: colors.border,
        borderBottomWidth: StyleSheet.hairlineWidth,
        backgroundColor: 'white'
    },
    searchBarContainerActive: {
        borderBottomColor: colors.green
    },
    searchImage: {
        height: 16,
        width: 16,
        margin: 14,
        marginLeft: 6,
        tintColor: 'gray'
    },
    searchImageActive: {
        tintColor: colors.green
    },
    textInput: {
        flex: 1,
        fontSize: 16,
        fontFamily: fonts.regular
    },
    cancelContainer: {
        padding: 8,
        backgroundColor: 'transparent'
    },
    cancelText: {
        color: colors.blue,
        fontSize: 16,
        fontFamily: fonts.regular
    }
});
//# sourceMappingURL=styles.js.map