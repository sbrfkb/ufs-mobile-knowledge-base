import React, { Component } from 'react';
import { LeftPageHeader, Page, Text, View } from 'ufs-mobile-platform';
import { Image, TextInput, TouchableOpacity } from 'react-native';
import { colors } from '../../../../common/constants';
import SearchScreen from '../../../search';
import styles from './styles';
export default class SearchComponent extends Component {
    constructor(props) {
        super(props);
        this._renderSearchBar = () => {
            const containerActiveStyle = this.state.isSearchBarActive ? styles.searchBarContainerActive : null;
            const searchImageActiveStyle = this.state.isSearchBarActive ? styles.searchImageActive : null;
            return (React.createElement(View, { style: [styles.searchBarContainer, containerActiveStyle] },
                React.createElement(Image, { source: require('./images/search.png'), style: [styles.searchImage, searchImageActiveStyle] }),
                React.createElement(TextInput, { placeholder: 'Найти', value: this.props.searchString, returnKeyType: 'search', enablesReturnKeyAutomatically: true, selectionColor: colors.green, clearButtonMode: 'always', autoCorrect: false, autoCapitalize: 'none', autoFocus: true, style: styles.textInput, onFocus: () => this.setState({ isSearchBarActive: true }), onEndEditing: () => this.setState({ isSearchBarActive: false }), onChange: (event) => this.props.onSearch(event.nativeEvent.text), keyboardAppearance: 'dark' }),
                React.createElement(TouchableOpacity, { onPress: this._onCancel, style: styles.cancelContainer },
                    React.createElement(Text, { style: styles.cancelText }, 'Отменить'))));
        };
        this._onCancel = () => {
            this.setState({ isSearchBarActive: false });
            this.props.onCloseButtonPressed();
        };
        this.state = {};
    }
    render() {
        return (React.createElement(Page, { content: React.createElement(SearchScreen, { isSearchBarActive: this.state.isSearchBarActive }) },
            React.createElement(LeftPageHeader, null, this._renderSearchBar())));
    }
}
//# sourceMappingURL=component.js.map