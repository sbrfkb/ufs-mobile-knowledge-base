import React from 'react';
import {StyleSheet} from 'react-native';

import {colors, fonts} from '../../../../common/constants';


export default StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: 'white'
    } as React.ViewStyle,

    searchBarContainer: {
        height: 44,
        width: 640,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 12,
        borderBottomColor: colors.border,
        borderBottomWidth: StyleSheet.hairlineWidth,
        backgroundColor: 'white'
    } as React.ViewStyle,

    searchBarContainerActive: {
        borderBottomColor: colors.green
    } as React.ViewStyle,

    searchImage: {
        height: 16,
        width: 16,
        margin: 14,
        marginLeft: 6,
        tintColor: 'gray'
    } as React.ImageStyle,

    searchImageActive: {
        tintColor: colors.green
    } as React.ImageStyle,

    textInput: {
        flex: 1,
        fontSize: 16,
        fontFamily: fonts.regular
    } as React.TextStyle,

    cancelContainer: {
        padding: 8,
        backgroundColor: 'transparent'
    } as React.ViewStyle,

    cancelText: {
        color: colors.blue,
        fontSize: 16,
        fontFamily: fonts.regular
    } as React.TextStyle

});