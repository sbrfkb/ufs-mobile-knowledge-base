import { connect } from 'react-redux';
import Component from './component';
import { closeSearchScreen, search } from '../../../search/actions';
const mapStateToProps = (state) => {
    return {
        searchString: state.user.knowledgeBase.screens.search.searchString
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        onSearch: (searchString) => {
            dispatch(search(searchString));
        },
        onCloseButtonPressed: () => {
            dispatch(closeSearchScreen());
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Component);
//# sourceMappingURL=index.js.map