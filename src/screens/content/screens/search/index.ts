import {connect} from 'react-redux';

import {IGlobalState} from '../../../../common/models';

import Component, {IProps} from './component';
import {closeSearchScreen, search} from '../../../search/actions';


const mapStateToProps = (state: IGlobalState): IProps => {
    return {
        searchString: state.user.knowledgeBase.screens.search.searchString
    };
};


const mapDispatchToProps = (dispatch): IProps => {
    return {
        onSearch: (searchString) => {
            dispatch(search(searchString));
        },
        onCloseButtonPressed: () => {
            dispatch(closeSearchScreen());
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Component);
