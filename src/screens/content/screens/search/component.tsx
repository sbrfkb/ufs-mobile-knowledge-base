import React, {Component} from 'react';
import {LeftPageHeader, Page, Text, View} from 'ufs-mobile-platform';
import {Image, TextInput, TouchableOpacity} from 'react-native';

import {colors} from '../../../../common/constants';
import SearchScreen from '../../../search';

import styles from './styles';


export interface IProps {
    searchString?: string

    onSearch?: (searchString: string) => void
    onCloseButtonPressed?: () => void
}

interface IState {
    isSearchBarActive?: boolean
}


export default class SearchComponent extends Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <Page content={<SearchScreen isSearchBarActive={this.state.isSearchBarActive}/>}>
                <LeftPageHeader>{this._renderSearchBar()}</LeftPageHeader>
            </Page>
        );
    }

    private _renderSearchBar = () => {
        const containerActiveStyle = this.state.isSearchBarActive ? styles.searchBarContainerActive : null;
        const searchImageActiveStyle = this.state.isSearchBarActive ? styles.searchImageActive : null;

        return (
            <View style={[styles.searchBarContainer, containerActiveStyle]}>
                <Image
                    source={require('./images/search.png')}
                    style={[styles.searchImage, searchImageActiveStyle]}
                />
                <TextInput
                    placeholder={'Найти'}
                    value={this.props.searchString}
                    returnKeyType={'search'}
                    enablesReturnKeyAutomatically={true}
                    selectionColor={colors.green}
                    clearButtonMode={'always'}
                    autoCorrect={false}
                    autoCapitalize={'none'}
                    autoFocus={true}
                    style={styles.textInput}
                    onFocus={() => this.setState({isSearchBarActive: true})}
                    onEndEditing={() => this.setState({isSearchBarActive: false})}
                    onChange={(event) => this.props.onSearch(event.nativeEvent.text)}
                    keyboardAppearance={'dark'}
                />
                <TouchableOpacity onPress={this._onCancel} style={styles.cancelContainer}>
                    <Text style={styles.cancelText}>{'Отменить'}</Text>
                </TouchableOpacity>
            </View>
        );
    };

    private _onCancel = () => {
        this.setState({isSearchBarActive: false});
        this.props.onCloseButtonPressed();
    };

}
