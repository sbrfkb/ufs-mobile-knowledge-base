import {connect} from 'react-redux';

import {IGlobalState} from '../../../../common/models';
import {StaticDocument} from '../../../../data/documents/models';

import Component, {IProps} from './component';
import {openSearchScreen} from '../../../search/actions';


const mapStateToProps = (state: IGlobalState): IProps => {
    const documentsState = state.user.knowledgeBase.data.documents;
    const treeState = state.user.knowledgeBase.data.tree;

    const selectedTreeItem = documentsState.itemsByID.get(treeState.selectedID);

    return {
        documentTitle: selectedTreeItem instanceof StaticDocument ? selectedTreeItem.title : null
    };
};


const mapDispatchToProps = (dispatch): IProps => {
    return {
        onSearchButtonPressed: () => {
            dispatch(openSearchScreen());
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Component);
