import React, { Component } from 'react';
import { LeftPageHeader, Page, H2, NavigationIconButton, NavigationIconButtonType, View } from 'ufs-mobile-platform';
import StaticScreen from '../../../document/static';
import styles from './styles';
export default class StaticComponent extends Component {
    render() {
        return (React.createElement(Page, { content: React.createElement(StaticScreen, null) },
            React.createElement(LeftPageHeader, null,
                React.createElement(View, { style: [styles.navBarContainer, this.props.documentTitle != undefined ? styles.navBarContainerHairline : {}] },
                    React.createElement(View, { style: styles.navBarTitleContainer },
                        React.createElement(H2, null, this.props.documentTitle)),
                    React.createElement(NavigationIconButton, { type: NavigationIconButtonType.SEARCH, onExecute: this.props.onSearchButtonPressed })))));
    }
}
//# sourceMappingURL=component.js.map