import React, {Component} from 'react';
import {
    LeftPageHeader,
    Page,
    H2,
    NavigationIconButton,
    NavigationIconButtonType,
    View
} from 'ufs-mobile-platform';

import StaticScreen from '../../../document/static';

import styles from './styles';


export interface IProps {
    documentTitle?: string

    onSearchButtonPressed?: () => void
}


export default class StaticComponent extends Component<IProps, any> {

    render() {
        return (
            <Page content={<StaticScreen />}>
                <LeftPageHeader>
                    <View style={[styles.navBarContainer, this.props.documentTitle != undefined ? styles.navBarContainerHairline : {}]}>
                        <View style={styles.navBarTitleContainer}>
                            <H2>{this.props.documentTitle}</H2>
                        </View>
                        <NavigationIconButton
                            type={NavigationIconButtonType.SEARCH}
                            onExecute={this.props.onSearchButtonPressed}
                        />
                    </View>
                </LeftPageHeader>
            </Page>
        );
    }

}
