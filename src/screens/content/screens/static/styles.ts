import React from 'react';
import {StyleSheet} from 'react-native';

import {colors} from '../../../../common/constants';


export default StyleSheet.create({

    navBarContainer: {
        flexDirection: 'row',
        height: 44,
        width: 640,
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingLeft: 10 + 44
    } as React.ViewStyle,

    navBarContainerHairline: {
        borderBottomColor: colors.border,
        borderBottomWidth: StyleSheet.hairlineWidth
    } as React.ViewStyle,

    navBarTitleContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 8
    } as React.ViewStyle

});
