import { StyleSheet } from 'react-native';
import { colors } from '../../../../common/constants';
export default StyleSheet.create({
    navBarContainer: {
        flexDirection: 'row',
        height: 44,
        width: 640,
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingLeft: 10 + 44
    },
    navBarContainerHairline: {
        borderBottomColor: colors.border,
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    navBarTitleContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 8
    }
});
//# sourceMappingURL=styles.js.map