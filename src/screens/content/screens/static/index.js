import { connect } from 'react-redux';
import { StaticDocument } from '../../../../data/documents/models';
import Component from './component';
import { openSearchScreen } from '../../../search/actions';
const mapStateToProps = (state) => {
    const documentsState = state.user.knowledgeBase.data.documents;
    const treeState = state.user.knowledgeBase.data.tree;
    const selectedTreeItem = documentsState.itemsByID.get(treeState.selectedID);
    return {
        documentTitle: selectedTreeItem instanceof StaticDocument ? selectedTreeItem.title : null
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        onSearchButtonPressed: () => {
            dispatch(openSearchScreen());
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Component);
//# sourceMappingURL=index.js.map