import { connect } from 'react-redux';
import Component from './component';
const mapStateToProps = (state) => {
    const screensState = state.user.knowledgeBase.screens;
    return {
        isSearchOpen: screensState.search.isOpen,
        isExternalDocumentOpen: screensState.externalDocument.isOpen
    };
};
const mapDispatchToProps = () => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(Component);
//# sourceMappingURL=index.js.map