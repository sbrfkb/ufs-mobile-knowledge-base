import {connect} from 'react-redux';

import Component, {IProps} from './component';
import {IGlobalState} from '../../common/models';


const mapStateToProps = (state: IGlobalState): IProps => {
    const screensState = state.user.knowledgeBase.screens;

    return {
        isSearchOpen: screensState.search.isOpen,
        isExternalDocumentOpen: screensState.externalDocument.isOpen
    };
};


const mapDispatchToProps = (): IProps => {
    return {};
};


export default connect(mapStateToProps, mapDispatchToProps)(Component);
