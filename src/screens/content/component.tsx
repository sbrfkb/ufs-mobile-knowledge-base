import React, {Component} from 'react';
import {Modal} from 'react-native';
import {View} from 'ufs-mobile-platform';

import ExternalScreen from '../document/external';

import SearchScreen from './screens/search';
import StaticScreen from './screens/static';


export interface IProps {
    isSearchOpen?: boolean
    isExternalDocumentOpen?: boolean
}


export default class ContentComponent extends Component<IProps, any> {

    render() {
        return (
            <View style={{flex: 1}}>
                <Modal
                    animationType={'fade'}
                    transparent={false}
                    visible={this.props.isExternalDocumentOpen}
                >
                    <ExternalScreen/>
                </Modal>
                {this._renderContent()}
            </View>
        );
    }

    private _renderContent = () => {
        if (this.props.isSearchOpen) {
            return <SearchScreen/>;
        } else {
            return <StaticScreen/>;
        }
    };

}
