import React, { Component } from 'react';
import { Modal } from 'react-native';
import { View } from 'ufs-mobile-platform';
import ExternalScreen from '../document/external';
import SearchScreen from './screens/search';
import StaticScreen from './screens/static';
export default class ContentComponent extends Component {
    constructor() {
        super(...arguments);
        this._renderContent = () => {
            if (this.props.isSearchOpen) {
                return React.createElement(SearchScreen, null);
            }
            else {
                return React.createElement(StaticScreen, null);
            }
        };
    }
    render() {
        return (React.createElement(View, { style: { flex: 1 } },
            React.createElement(Modal, { animationType: 'fade', transparent: false, visible: this.props.isExternalDocumentOpen },
                React.createElement(ExternalScreen, null)),
            this._renderContent()));
    }
}
//# sourceMappingURL=component.js.map