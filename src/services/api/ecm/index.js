import { download as UFSDownload, get as UFSGet, post as UFSPost } from 'ufs-mobile-platform';
import APIConfig from './config';
const buildURL = (baseURL, endPoint, payload) => {
    let params = Object.keys(payload).map(function (key) {
        return key + '=' + encodeURIComponent(payload[key]);
    }).join('&');
    if (params.length > 0) {
        params = `?${params}`;
    }
    return `${baseURL}${endPoint}${params}`;
};
const buildHeaders = () => {
    return Object.assign({ 'Content-Type': 'application/json' }, APIConfig.sharedInstance.headers);
};
export const get = (endPoint, payload = {}) => {
    return UFSGet(buildURL(APIConfig.sharedInstance.baseURL, endPoint, payload), APIConfig.sharedInstance.headers);
};
export const post = (endPoint, data, payload = {}) => {
    return UFSPost(buildURL(APIConfig.sharedInstance.baseURL, endPoint, payload), data, buildHeaders());
};
export const getTFS = (endPoint, payload = {}) => {
    return UFSGet(buildURL(APIConfig.sharedInstance.tfsBaseURL, endPoint, payload), buildHeaders());
};
export const postTFS = (endPoint, data, payload = {}) => {
    return UFSPost(buildURL(APIConfig.sharedInstance.tfsBaseURL, endPoint, payload), data, buildHeaders());
};
export const download = (endPoint, payload = {}) => {
    return UFSDownload(buildURL(APIConfig.sharedInstance.tfsBaseDownloadURL, endPoint, payload), buildHeaders());
};
//# sourceMappingURL=index.js.map