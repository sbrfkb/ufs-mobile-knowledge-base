export default class ECMAPIConfig {

    baseURL: string;
    tfsBaseURL: string;
    tfsBaseDownloadURL: string;
    scenarioID: string;
    target: string;
    headers?: any;

    private static _sharedInstance?: ECMAPIConfig;

    static get sharedInstance(): ECMAPIConfig {
        if (!ECMAPIConfig._sharedInstance) {
            ECMAPIConfig._sharedInstance = new ECMAPIConfig();
        }
        return ECMAPIConfig._sharedInstance;
    }

}


export const configure = (baseURL: string, tfsBaseURL: string, tfsBaseDownloadURL: string, scenarioID: string, target: string, headers?: any) => {
    if (!baseURL || baseURL.length === 0) {
        throw new Error('Необходимо указать baseURL');
    }

    if (!tfsBaseURL || tfsBaseURL.length === 0) {
        throw new Error('Необходимо указать tfsBaseURL');
    }

    if (!tfsBaseDownloadURL || tfsBaseDownloadURL.length === 0) {
        throw new Error('Необходимо указать tfsBaseDownloadURL');
    }

    if (!scenarioID || scenarioID.length === 0) {
        throw new Error('Необходимо указать scenarioID');
    }

    if (!target || target.length === 0) {
        throw new Error('Необходимо указать target');
    }

    ECMAPIConfig.sharedInstance.baseURL = baseURL;
    ECMAPIConfig.sharedInstance.tfsBaseURL = tfsBaseURL;
    ECMAPIConfig.sharedInstance.tfsBaseDownloadURL = tfsBaseDownloadURL;
    ECMAPIConfig.sharedInstance.scenarioID = scenarioID;
    ECMAPIConfig.sharedInstance.target = target;
    ECMAPIConfig.sharedInstance.headers = headers;
};
