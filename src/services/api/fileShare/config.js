export default class FileShareAPIConfig {
    static get sharedInstance() {
        if (!FileShareAPIConfig._sharedInstance) {
            FileShareAPIConfig._sharedInstance = new FileShareAPIConfig();
        }
        return FileShareAPIConfig._sharedInstance;
    }
}
export const configure = (apiURL, project) => {
    if (!apiURL || apiURL.length === 0) {
        throw new Error('Необходимо указать URL АПИ');
    }
    if (!project || project.length === 0) {
        throw new Error('Необходимо указать проект');
    }
    FileShareAPIConfig.sharedInstance.baseURL = apiURL;
    FileShareAPIConfig.sharedInstance.project = project;
};
//# sourceMappingURL=config.js.map