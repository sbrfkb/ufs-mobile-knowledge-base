export default class FileShareAPIConfig {

    baseURL: string;
    project: string;

    private static _sharedInstance?: FileShareAPIConfig;

    static get sharedInstance(): FileShareAPIConfig {
        if (!FileShareAPIConfig._sharedInstance) {
            FileShareAPIConfig._sharedInstance = new FileShareAPIConfig();
        }
        return FileShareAPIConfig._sharedInstance;
    }

}


export const configure = (apiURL: string, project: string) => {
    if (!apiURL || apiURL.length === 0) {
        throw new Error('Необходимо указать URL АПИ');
    }

    if (!project || project.length === 0) {
        throw new Error('Необходимо указать проект');
    }

    FileShareAPIConfig.sharedInstance.baseURL = apiURL;
    FileShareAPIConfig.sharedInstance.project = project;
};
