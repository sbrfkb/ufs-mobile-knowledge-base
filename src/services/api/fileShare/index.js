import { download as UFSDownload, get as UFSGet } from 'ufs-mobile-platform';
import APIConfig from './config';
const buildURL = (endPoint, needProjectInfo, payload) => {
    if (needProjectInfo) {
        payload['requestDir'] = APIConfig.sharedInstance.project;
    }
    let params = Object.keys(payload).map(function (key) {
        return key + '=' + encodeURIComponent(payload[key]);
    }).join('&');
    if (params.length > 0) {
        params = `?${params}`;
    }
    return `${APIConfig.sharedInstance.baseURL}${endPoint}${params}`;
};
export const get = (endPoint, needProjectInfo = true, payload = {}) => {
    return UFSGet(buildURL(endPoint, needProjectInfo, payload));
};
export const getPlainText = (endPoint, needProjectInfo = true, payload = {}) => {
    return fetch(buildURL(endPoint, needProjectInfo, payload)).then(r => r.text());
};
export const download = (endPoint, needProjectInfo = true, payload = {}) => {
    return UFSDownload(buildURL(endPoint, needProjectInfo, payload));
};
//# sourceMappingURL=index.js.map