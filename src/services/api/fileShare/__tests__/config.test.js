import APIConfig, { configure } from '../config';
describe('configure', () => {
    beforeEach(() => {
        APIConfig.sharedInstance.project = undefined;
        APIConfig.sharedInstance.baseURL = undefined;
    });
    it('должно выставлять оба параметра', () => {
        configure('http://api.ya.ru', 'cib');
        expect(APIConfig.sharedInstance.baseURL).toBe('http://api.ya.ru');
        expect(APIConfig.sharedInstance.project).toBe('cib');
    });
    it('параметры должны быть заполнены', () => {
        expect(() => {
            configure(undefined, undefined);
        }).toThrowError('Необходимо указать URL АПИ');
        expect(() => {
            configure(undefined, 'cib');
        }).toThrowError('Необходимо указать URL АПИ');
        expect(() => {
            configure('http://api.ya.ru', undefined);
        }).toThrowError('Необходимо указать проект');
    });
});
//# sourceMappingURL=config.test.js.map