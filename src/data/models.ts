import {ITreeState} from './tree/models';
import {IDocumentsState} from './documents/models';


export interface IDataState {
    tree: ITreeState
    documents: IDocumentsState
}
