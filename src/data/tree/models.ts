export interface ITreeState {
    fetching: boolean
    error?: Error

    rootID?: string
    selectedID?: string
}


export interface ITreeItem {
    id: string;
    title: string;
    path?: string;
    parentID?: string;
}


export class Group implements ITreeItem {
    id: string;
    title: string;
    path?: string;
    parentID?: string;
    panelIndex?: number;
}
