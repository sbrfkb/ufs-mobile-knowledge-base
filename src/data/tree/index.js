import * as documentsActionCreators from '../documents/actions';
import { SplitPanelActions } from 'ufs-mobile-platform';
import { openExternalDocumentScreen } from '../../screens/document/external/actions';
import { ExternalDocument, StaticDocument } from '../documents/models';
import { openStaticDocument } from '../documents/index';
import * as actionCreators from './actions';
import api from './api';
import { Group } from './models';
export const loadTree = () => {
    return (dispatch) => {
        dispatch(actionCreators.loadRequest.begin());
        api.loadTree()
            .then((treeInfo) => {
            dispatch(actionCreators.receive(treeInfo.rootID));
            dispatch(documentsActionCreators.receive(treeInfo.itemsByID));
            dispatch(actionCreators.loadRequest.end());
        })
            .catch((error) => {
            dispatch(actionCreators.loadRequest.failure(error));
        });
    };
};
export const select = (item) => {
    return (dispatch) => {
        if (item instanceof StaticDocument) {
            dispatch(actionCreators.select(item.id));
        }
        else {
            dispatch(actionCreators.select(null));
        }
        dispatch(openTreeItem(item));
    };
};
export const openTreeItem = (item) => {
    return (dispatch) => {
        if (item instanceof Group) {
            dispatch(SplitPanelActions.pushAccessory(item.panelIndex, 'knowledgeBasePanel'));
        }
        else if (item instanceof StaticDocument) {
            dispatch(openStaticDocument(item));
        }
        else if (item instanceof ExternalDocument) {
            dispatch(openExternalDocumentScreen(item));
        }
    };
};
//# sourceMappingURL=index.js.map