import {IAction} from '../../common/models';

import * as actionTypes from './actionTypes';
import {ITreeState} from './models';


const initialState = {
    fetching: true,
} as ITreeState;


export const reducer = (state = initialState, action: IAction): ITreeState => {
    switch (action.type) {

        case actionTypes.LOAD_REQUEST.BEGIN:
            return {
                ...state,
                fetching: true
            };

        case actionTypes.LOAD_REQUEST.END:
            return {
                ...state,
                fetching: false
            };

        case actionTypes.LOAD_REQUEST.FAILURE:
            return {
                ...state,
                fetching: false,
                error: action.payload.error
            };

        case actionTypes.RECEIVE:
            return {
                ...state,
                rootID: action.payload.rootID
            };

        case actionTypes.SELECT:
            return {
                ...state,
                selectedID: action.payload.id
            };

        default:
            return state;
    }
};
