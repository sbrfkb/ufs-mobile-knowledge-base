import {reducer} from '../reducer';
import * as actionTypes from '../actionTypes';


describe('tree:reducer', () => {

    it('должен возвращать начальный стейт', () => {
        const action = {
            type: 'test action'
        };
        const initialState = {
            fetching: true,
        };

        expect(reducer(initialState, action)).toEqual(initialState);
    });


    it('RECEIVE', () => {
        const initialState = {
            fetching: true,
        };

        const action = {
            type: actionTypes.RECEIVE,
            payload: {
                rootID: '1'
            }
        };
        const expectedState = {
            fetching: true,
            rootID: '1'
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    describe('LOAD_REQUEST', () => {

        it('BEGIN', () => {
            const initialState = {
                fetching: true
            };
            const action = {
                type: actionTypes.LOAD_REQUEST.BEGIN
            };
            const expectedState = {
                fetching: true
            };

            expect(reducer(initialState, action)).toEqual(expectedState);
        });

        it('END', () => {
            const initialState = {
                fetching: true
            };
            const action = {
                type: actionTypes.LOAD_REQUEST.END
            };
            const expectedState = {
                fetching: false
            };

            expect(reducer(initialState, action)).toEqual(expectedState);
        });

        it('FAILURE', () => {
            const initialState = {
                fetching: true,
            };
            const error = new Error('error load content');
            const action = {
                type: actionTypes.LOAD_REQUEST.FAILURE,
                payload: {
                    error
                }
            };
            const expectedState = {
                fetching: false,
                error
            };

            expect(reducer(initialState, action)).toEqual(expectedState);
        });

    });

    it('SELECT', () => {
        const initialState = {
            fetching: false
        };
        const action = {
            type: actionTypes.SELECT,
            payload: {
                id: '1'
            }
        };
        const expectedState = {
            fetching: false,
            selectedID: '1'
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });

});
