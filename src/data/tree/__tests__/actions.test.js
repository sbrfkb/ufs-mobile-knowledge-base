import * as actions from '../actions';
import * as actionTypes from '../actionTypes';
describe('action', () => {
    it('receive', () => {
        const rootID = '1';
        const expectedAction = {
            type: actionTypes.RECEIVE,
            payload: {
                rootID
            }
        };
        expect(actions.receive(rootID)).toEqual(expectedAction);
    });
    it('select', () => {
        const itemID = '1';
        const expectedAction = {
            type: actionTypes.SELECT,
            payload: {
                id: itemID
            }
        };
        expect(actions.select(itemID)).toEqual(expectedAction);
    });
    describe('loadRequest', () => {
        it('begin', () => {
            const expectedAction = {
                type: actionTypes.LOAD_REQUEST.BEGIN
            };
            expect(actions.loadRequest.begin()).toEqual(expectedAction);
        });
        it('end', () => {
            const expectedAction = {
                type: actionTypes.LOAD_REQUEST.END
            };
            expect(actions.loadRequest.end()).toEqual(expectedAction);
        });
        it('failure', () => {
            const error = new Error('Error');
            const expectedAction = {
                type: actionTypes.LOAD_REQUEST.FAILURE,
                payload: {
                    error
                }
            };
            expect(actions.loadRequest.failure(error)).toEqual(expectedAction);
        });
    });
});
//# sourceMappingURL=actions.test.js.map