import { childs, filterGroups } from '../selectors';
import { Group } from '../models';
import { ExternalDocument, StaticDocument } from '../../documents/models';
const documentsByID = new Map();
const rootGroup = new Group();
rootGroup.id = 'rootGroup';
rootGroup.path = 'rootGroup';
rootGroup.title = 'Root Group';
documentsByID.set(rootGroup.id, rootGroup);
const group1 = new Group();
group1.id = '1';
group1.path = 'group1';
group1.title = 'Group 1';
group1.parentID = rootGroup.id;
documentsByID.set(group1.id, group1);
const group2 = new Group();
group2.id = '2';
group2.path = 'group2';
group2.title = 'Group 2';
group2.parentID = rootGroup.id;
documentsByID.set(group2.id, group2);
const group3 = new Group();
group3.id = '3';
group3.path = 'group3';
group3.title = 'Group 3';
group3.parentID = group1.id;
documentsByID.set(group3.id, group3);
const document1TXT = new StaticDocument();
document1TXT.id = 'd1';
document1TXT.path = 'document1.txt';
document1TXT.title = 'Document 1';
document1TXT.parentID = rootGroup.id;
documentsByID.set(document1TXT.id, document1TXT);
const document1PDF = new ExternalDocument();
document1PDF.id = 'dp1';
document1PDF.path = 'document1.pdf';
document1PDF.title = 'Document 1';
document1PDF.parentID = document1TXT.id;
documentsByID.set(document1PDF.id, document1PDF);
const document2TXT = new StaticDocument();
document2TXT.id = 'd2';
document2TXT.path = 'document2.txt';
document2TXT.title = 'Document 2';
document2TXT.parentID = rootGroup.id;
documentsByID.set(document2TXT.id, document2TXT);
describe('childs', () => {
    it('все хорошо', () => {
        expect(childs(documentsByID, rootGroup.id)).toEqual([group1, group2, document1TXT, document2TXT]);
        expect(childs(documentsByID, document1TXT.id)).toEqual([document1PDF]);
        expect(childs(documentsByID, group1.id)).toEqual([group3]);
    });
    it('нет детей', () => {
        expect(childs(documentsByID, document2TXT.id)).toEqual([]);
        expect(childs(documentsByID, group2.id)).toEqual([]);
    });
});
describe('filterGroups', () => {
    it('все хорошо', () => {
        let expectedResults = [
            rootGroup,
            group3,
            group1,
            group2
        ];
        expectedResults = expectedResults.map((g, index) => {
            g.panelIndex = index;
            return g;
        });
        expect(filterGroups(documentsByID)).toEqual(expectedResults);
    });
});
//# sourceMappingURL=selectors.test.js.map