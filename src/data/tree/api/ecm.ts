import {post} from '../../../services/api/ecm/index';
import {ExternalDocument, StaticDocument} from '../../documents/models';
import {Group, ITreeItem} from '../models';
import APIConfig from '../../../services/api/ecm/config';

import {ITreeAPI} from './models';


class TreeECMAPI implements ITreeAPI {

    loadTree(): Promise<{ rootID, itemsByID }> {
        const data = {
            traverseTree: {
                target: APIConfig.sharedInstance.target,
                depth: 99
            }
        };
        return post('/file-base/tree', data)
            .then((json: any) => {
                const item = json['body'];
                return {
                    rootID: TreeECMAPI.treeItemFactory(item).id,
                    itemsByID: TreeECMAPI.treeItemsByID(item)
                };
            })
            .catch(e => {
                console.log(e);
            });
    }

    private static treeItemFactory(object: any): ITreeItem {
        let treeItem: ITreeItem;

        if (object['folder']) {
            let attrs = TreeECMAPI.flatAttributes(object['folder']['attrs']);

            if (attrs['ProductInfo'] === undefined) {
                treeItem = new Group();
            } else {
                const document = new StaticDocument();
                document.content = attrs['ProductInfo'];

                treeItem = document;
            }

            treeItem.id = object['folder']['id'];
            treeItem.title = object['folder']['name'];
        } else if (object['document']) {
            let attrs = TreeECMAPI.flatAttributes(object['document']['attrs']);

            treeItem = new ExternalDocument();
            treeItem.id = object['document']['id'];
            treeItem.title = attrs['ThemaPresent'];
        }

        return treeItem;
    }

    private static flatAttributes(attrs: any[]) {
        let result = [];

        if (attrs && attrs.length > 0) {
            attrs.forEach((attr) => {
                const value = TreeECMAPI.extractValue(attr);
                result[value['id']] = value['value'];
            });
        }

        return result;
    }

    private static treeItemsByID(rootTreeItem: any): Map<string, ITreeItem> {
        let items: any[] = [];
        let itemsByID = new Map<string, ITreeItem>();
        let index = 0;

        items.push(TreeECMAPI.extractValue(rootTreeItem));
        let model = TreeECMAPI.treeItemFactory(rootTreeItem);
        itemsByID.set(model.id, model);

        while (index < items.length) {
            if (items[index]['objects']) {
                items[index]['objects'].forEach((child: ITreeItem) => {
                    let model = TreeECMAPI.treeItemFactory(child);
                    model.parentID = items[index]['id'];
                    itemsByID.set(model.id, model);

                    items.push(TreeECMAPI.extractValue(child));
                });
            }
            index++;
        }

        return itemsByID;
    }

    private static extractValue(obj) {
        return Object.values(obj)[0];
    }

}

const api = new TreeECMAPI();

export default api;
