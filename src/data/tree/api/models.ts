export interface ITreeAPI {
    loadTree(): Promise<{rootID, itemsByID}>
}
