import { get } from '../../../services/api/fileShare/index';
import { ExternalDocument, StaticDocument } from '../../documents/models';
import { Group } from '../models';
class TreeFileShareAPI {
    loadTree() {
        return get('/tree', true)
            .then((json) => {
            const item = json['body'];
            return {
                rootID: TreeFileShareAPI.treeItemFactory(item).id,
                itemsByID: TreeFileShareAPI.treeItemsByID(item)
            };
        });
    }
    static treeItemFactory(object) {
        if (object['dir']) {
            let group = new Group();
            group.id = `${object['dir']}_${object['path']}`;
            group.title = object['name'];
            group.path = object['path'];
            return group;
        }
        else if (object['path'].endsWith('.pdf')) {
            let document = new ExternalDocument();
            document.id = `${object['dir']}_${object['path']}`;
            document.title = object['name'];
            document.path = object['path'];
            return document;
        }
        else {
            let document = new StaticDocument();
            document.id = `${object['dir']}_${object['path']}`;
            document.title = object['name'];
            document.path = object['path'];
            return document;
        }
    }
    static treeItemsByID(rootTreeItem) {
        let items = [];
        let itemsByID = new Map();
        let index = 0;
        items.push(rootTreeItem);
        let model = TreeFileShareAPI.treeItemFactory(rootTreeItem);
        itemsByID.set(model.id, model);
        while (index < items.length) {
            if (items[index]['childs']) {
                items[index]['childs'].forEach((child) => {
                    let model = TreeFileShareAPI.treeItemFactory(child);
                    model.parentID = `${items[index]['dir']}_${items[index]['path']}`;
                    itemsByID.set(model.id, model);
                    items.push(child);
                });
            }
            index++;
        }
        return itemsByID;
    }
}
const api = new TreeFileShareAPI();
export default api;
//# sourceMappingURL=fileShare.js.map