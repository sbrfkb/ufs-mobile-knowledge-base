import { Group } from './models';
export const filterGroups = (itemsByID) => {
    let groups = [];
    // Оставляем только группы
    itemsByID.forEach((treeItem) => {
        if (treeItem instanceof Group) {
            groups.push(treeItem);
        }
    });
    // Первой должна идти рутовая группа
    groups = groups.sort((a, b) => {
        if (a.parentID === undefined || (a.parentID < b.parentID) || (a.parentID === b.parentID && a.title < b.title)) {
            return -1;
        }
        else {
            return 1;
        }
    });
    // Проставляем индексы панелей
    groups = groups.map((group, index) => {
        group.panelIndex = index;
        return group;
    });
    return groups;
};
export const childs = (itemsByID, itemID) => {
    let items = [];
    itemsByID.forEach((treeItem) => {
        if (treeItem.parentID != itemID) {
            return;
        }
        items.push(treeItem);
    });
    return items;
};
//# sourceMappingURL=selectors.js.map