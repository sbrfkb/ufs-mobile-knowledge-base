import {Group, ITreeItem} from './models';


export const filterGroups = (itemsByID: Map<string, ITreeItem>) => {
    let groups: Group[] = [];

    // Оставляем только группы
    itemsByID.forEach((treeItem: ITreeItem) => {
        if (treeItem instanceof Group) {
            groups.push(treeItem);
        }
    });

    // Первой должна идти рутовая группа
    groups = groups.sort((a, b): number => {
        if (a.parentID === undefined || (a.parentID < b.parentID) || (a.parentID === b.parentID && a.title < b.title)) {
            return -1;
        } else {
            return 1;
        }
    });

    // Проставляем индексы панелей
    groups = groups.map((group: Group, index: number) => {
        group.panelIndex = index;
        return group;
    });

    return groups;
};


export const childs = (itemsByID: Map<string, ITreeItem>, itemID: string) => {
    let items: ITreeItem[] = [];

    itemsByID.forEach((treeItem: ITreeItem) => {
        if (treeItem.parentID != itemID) {
            return;
        }

        items.push(treeItem);
    });

    return items;
};
