import * as actionTypes from './actionTypes';


export const receive = (rootID: string) => ({
    type: actionTypes.RECEIVE,
    payload: {
        rootID
    }
});

export const select = (itemID: string) => ({
    type: actionTypes.SELECT,
    payload: {
        id: itemID
    }
});


export const loadRequest = {
    begin: () => ({
        type: actionTypes.LOAD_REQUEST.BEGIN
    }),
    end: () => ({
        type: actionTypes.LOAD_REQUEST.END
    }),
    failure: (error) => ({
        type: actionTypes.LOAD_REQUEST.FAILURE,
        payload: {
            error
        }
    })
};
