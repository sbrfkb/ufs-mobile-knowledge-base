import * as actionTypes from './actionTypes';
const initialState = {
    fetching: true,
};
export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOAD_REQUEST.BEGIN:
            return Object.assign({}, state, { fetching: true });
        case actionTypes.LOAD_REQUEST.END:
            return Object.assign({}, state, { fetching: false });
        case actionTypes.LOAD_REQUEST.FAILURE:
            return Object.assign({}, state, { fetching: false, error: action.payload.error });
        case actionTypes.RECEIVE:
            return Object.assign({}, state, { rootID: action.payload.rootID });
        case actionTypes.SELECT:
            return Object.assign({}, state, { selectedID: action.payload.id });
        default:
            return state;
    }
};
//# sourceMappingURL=reducer.js.map