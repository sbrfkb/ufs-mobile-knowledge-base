import {ITreeItem} from '../tree/models';

import * as actionTypes from './actionTypes';


export const receive = (itemsByID: Map<string, ITreeItem>) => ({
    type: actionTypes.RECEIVE,
    payload: {
        itemsByID
    }
});


export const update = (item: ITreeItem) => ({
    type: actionTypes.UPDATE,
    payload: {
        item
    }
});


export const staticDocument = {
    loadContentRequest: {
        begin: () => ({
            type: actionTypes.LOAD_CONTENT_REQUEST.BEGIN
        }),
        end: () => ({
            type: actionTypes.LOAD_CONTENT_REQUEST.END
        }),
        failure: (error) => ({
            type: actionTypes.LOAD_CONTENT_REQUEST.FAILURE,
            payload: {
                error
            }
        })
    }
};
