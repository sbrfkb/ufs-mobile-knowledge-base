class DocumentsECMAPI {
    loadStaticDocumentContent(document) {
        return Promise.resolve(document.content);
    }
}
const api = new DocumentsECMAPI();
export default api;
//# sourceMappingURL=ecm.js.map