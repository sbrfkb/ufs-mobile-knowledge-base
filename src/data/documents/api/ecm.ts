import {StaticDocument} from '../models';

import {IDocumentsAPI} from './models';


class DocumentsECMAPI implements IDocumentsAPI {

    loadStaticDocumentContent(document: StaticDocument): Promise<string> {
        return Promise.resolve(document.content);
    }

}

const api = new DocumentsECMAPI();

export default api;
