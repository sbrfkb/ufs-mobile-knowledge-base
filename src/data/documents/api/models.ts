import {StaticDocument} from '../models';


export interface IDocumentsAPI {
    loadStaticDocumentContent(document: StaticDocument): Promise<string>
}
