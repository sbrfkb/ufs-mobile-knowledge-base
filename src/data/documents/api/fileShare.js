import { getPlainText } from '../../../services/api/fileShare/index';
class DocumentsFileShareAPI {
    loadStaticDocumentContent(document) {
        return getPlainText('/content', true, { path: document.path });
    }
}
const api = new DocumentsFileShareAPI();
export default api;
//# sourceMappingURL=fileShare.js.map