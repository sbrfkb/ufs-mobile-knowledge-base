import {getPlainText} from '../../../services/api/fileShare/index';
import {StaticDocument} from '../models';

import {IDocumentsAPI} from './models';


class DocumentsFileShareAPI implements IDocumentsAPI {

    loadStaticDocumentContent(document: StaticDocument): Promise<string> {
        return getPlainText('/content', true, {path: document.path});
    }

}

const api = new DocumentsFileShareAPI();

export default api;
