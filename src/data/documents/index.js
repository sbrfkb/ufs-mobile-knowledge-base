import * as actionCreators from './actions';
import api from './api/index';
export const updateDocument = (item) => {
    return (dispatch) => {
        dispatch(actionCreators.update(item));
    };
};
export const openStaticDocument = (document) => {
    return (dispatch) => {
        dispatch(actionCreators.staticDocument.loadContentRequest.begin());
        api.loadStaticDocumentContent(document)
            .then((content) => {
            document.content = content;
            dispatch(updateDocument(document));
            dispatch(actionCreators.staticDocument.loadContentRequest.end());
        })
            .catch((error) => {
            dispatch(actionCreators.staticDocument.loadContentRequest.failure(error));
        });
    };
};
//# sourceMappingURL=index.js.map