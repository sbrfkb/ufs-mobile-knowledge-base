import * as treeActionTypes from '../tree/actionTypes';
import * as actionTypes from './actionTypes';
const initialState = {
    fetching: false,
    itemsByID: new Map()
};
export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.RECEIVE:
            return Object.assign({}, state, { itemsByID: action.payload.itemsByID });
        case actionTypes.UPDATE: {
            const itemsByID = state.itemsByID;
            const item = action.payload.item;
            itemsByID.set(item.id, item);
            return Object.assign({}, state, { itemsByID });
        }
        case actionTypes.LOAD_CONTENT_REQUEST.BEGIN:
            return Object.assign({}, state, { fetching: true });
        case actionTypes.LOAD_CONTENT_REQUEST.END:
        case treeActionTypes.SELECT:
            return Object.assign({}, state, { fetching: false });
        case actionTypes.LOAD_CONTENT_REQUEST.FAILURE:
            return Object.assign({}, state, { fetching: false, error: action.payload.error });
        default:
            return state;
    }
};
//# sourceMappingURL=reducer.js.map