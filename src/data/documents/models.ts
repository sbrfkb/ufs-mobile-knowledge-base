import {ITreeItem} from '../tree/models';


export interface IDocumentsState {
    fetching: boolean
    error?: Error
    itemsByID?: Map<string, ITreeItem>
}


export class Document implements ITreeItem {
    id: string;
    title: string;
    path?: string;
    parentID?: string;
    isFavorite: boolean = false;
}

export class ExternalDocument extends Document {
    fileID?: string;
}

export class StaticDocument extends Document {
    sections: StaticDocumentSection[];
    content?: string;
}

export class StaticDocumentSection {
    template: string;
    title: string;
    data: any;
}
