export class Document {
    constructor() {
        this.isFavorite = false;
    }
}
export class ExternalDocument extends Document {
}
export class StaticDocument extends Document {
}
export class StaticDocumentSection {
}
//# sourceMappingURL=models.js.map