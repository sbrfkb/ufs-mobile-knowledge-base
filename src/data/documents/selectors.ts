import {ITreeItem} from '../tree/models';

import {Document} from './models';


export const filterFavorites = (itemsByID: Map<string, ITreeItem>) => {
    let documents: Document[] = [];

    itemsByID.forEach((treeItem: ITreeItem) => {
        if (treeItem instanceof Document && treeItem.isFavorite) {
            documents.push(treeItem);
        }
    });

    return documents;
};
