import { Document } from './models';
export const filterFavorites = (itemsByID) => {
    let documents = [];
    itemsByID.forEach((treeItem) => {
        if (treeItem instanceof Document && treeItem.isFavorite) {
            documents.push(treeItem);
        }
    });
    return documents;
};
//# sourceMappingURL=selectors.js.map