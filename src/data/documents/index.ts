import {ITreeItem} from '../tree/models';

import * as actionCreators from './actions';
import api from './api/index';
import {StaticDocument} from './models';


export const updateDocument = (item: ITreeItem) => {
    return (dispatch) => {
        dispatch(actionCreators.update(item));
    };
};

export const openStaticDocument = (document: StaticDocument) => {
    return (dispatch) => {
        dispatch(actionCreators.staticDocument.loadContentRequest.begin());
        api.loadStaticDocumentContent(document)
            .then((content) => {
                document.content = content;
                dispatch(updateDocument(document));
                dispatch(actionCreators.staticDocument.loadContentRequest.end());
            })
            .catch((error) => {
                dispatch(actionCreators.staticDocument.loadContentRequest.failure(error));
            });
    };
};
