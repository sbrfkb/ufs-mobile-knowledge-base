import {IAction} from '../../common/models';
import {ITreeItem} from '../tree/models';
import * as treeActionTypes from '../tree/actionTypes';

import * as actionTypes from './actionTypes';
import {IDocumentsState} from './models';


const initialState = {
    fetching: false,
    itemsByID: new Map<string, ITreeItem>()
} as IDocumentsState;


export const reducer = (state = initialState, action: IAction): IDocumentsState => {
    switch (action.type) {

        case actionTypes.RECEIVE:
            return {
                ...state,
                itemsByID: action.payload.itemsByID
            };

        case actionTypes.UPDATE: {
            const itemsByID = state.itemsByID;
            const item = action.payload.item;
            itemsByID.set(item.id, item);
            return {
                ...state,
                itemsByID
            };
        }

        case actionTypes.LOAD_CONTENT_REQUEST.BEGIN:
            return {
                ...state,
                fetching: true
            };

        case actionTypes.LOAD_CONTENT_REQUEST.END:
        case treeActionTypes.SELECT:
            return {
                ...state,
                fetching: false
            };

        case actionTypes.LOAD_CONTENT_REQUEST.FAILURE:
            return {
                ...state,
                fetching: false,
                error: action.payload.error
            };

        default:
            return state;
    }
};
