import * as actions from '../actions';
import * as actionTypes from '../actionTypes';
import {ITreeItem} from '../../tree/models';


describe('documents:actions', () => {
    it('receive', () => {
        const itemsByID = new Map<string, ITreeItem>();
        itemsByID.set('1', undefined);
        const expectedAction = {
            type: actionTypes.RECEIVE,
            payload: {
                itemsByID
            }
        };
        expect(actions.receive(itemsByID)).toEqual(expectedAction);
    });

    it('update', () => {
        const item: ITreeItem = {
            id: '1',
            title: 'Title',
            path: 'Path'
        };
        const expectedAction = {
            type: actionTypes.UPDATE,
            payload: {
                item
            }
        };
        expect(actions.update(item)).toEqual(expectedAction);
    });

    describe('staticDocument.loadContentRequest', () => {
        it('begin', () => {
            const expectedAction = {
                type: actionTypes.LOAD_CONTENT_REQUEST.BEGIN
            };
            expect(actions.staticDocument.loadContentRequest.begin()).toEqual(expectedAction);
        });
        it('end', () => {
            const expectedAction = {
                type: actionTypes.LOAD_CONTENT_REQUEST.END
            };
            expect(actions.staticDocument.loadContentRequest.end()).toEqual(expectedAction);
        });
        it('failure', () => {
            const error = new Error('Error');
            const expectedAction = {
                type: actionTypes.LOAD_CONTENT_REQUEST.FAILURE,
                payload: {
                    error
                }
            };
            expect(actions.staticDocument.loadContentRequest.failure(error)).toEqual(expectedAction);
        });
    });
});
