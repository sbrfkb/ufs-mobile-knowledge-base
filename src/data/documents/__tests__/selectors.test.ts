import {filterFavorites} from '../selectors';
import {Group, ITreeItem} from '../../tree/models';
import {ExternalDocument, StaticDocument} from '../models';


describe('filterFavorites', () => {

    it('хорошие данные', () => {
        const documentsByID = new Map<string, ITreeItem>();

        const document1PDF = new ExternalDocument();
        document1PDF.id = 'dp1';
        document1PDF.path = 'document1.pdf';
        document1PDF.title = 'Document 1';
        documentsByID.set(document1PDF.id, document1PDF);

        const document2TXT = new StaticDocument();
        document2TXT.id = 'd2';
        document2TXT.path = 'document2.txt';
        document2TXT.title = 'Document 2';
        document2TXT.isFavorite = true;
        documentsByID.set(document2TXT.id, document2TXT);

        const document3PDF = new ExternalDocument();
        document3PDF.id = 'dp3';
        document3PDF.path = 'document3.pdf';
        document3PDF.title = 'Document 3';
        document3PDF.isFavorite = true;
        documentsByID.set(document3PDF.id, document3PDF);

        const group1 = new Group();
        group1.id = '1';
        group1.path = 'group1';
        group1.title = 'Group 1';
        documentsByID.set(group1.id, group1);

        expect(filterFavorites(documentsByID)).toEqual([document2TXT, document3PDF]);
    });

    it('нет избранного', () => {
        const documentsByID = new Map<string, ITreeItem>();

        const document1PDF = new ExternalDocument();
        document1PDF.id = 'dp1';
        document1PDF.path = 'document1.pdf';
        document1PDF.title = 'Document 1';
        documentsByID.set(document1PDF.id, document1PDF);

        const document2TXT = new StaticDocument();
        document2TXT.id = 'd2';
        document2TXT.path = 'document2.txt';
        document2TXT.title = 'Document 2';
        documentsByID.set(document2TXT.id, document2TXT);

        const document3PDF = new ExternalDocument();
        document3PDF.id = 'dp3';
        document3PDF.path = 'document3.pdf';
        document3PDF.title = 'Document 3';
        documentsByID.set(document3PDF.id, document3PDF);

        const group1 = new Group();
        group1.id = '1';
        group1.path = 'group1';
        group1.title = 'Group 1';
        documentsByID.set(group1.id, group1);

        expect(filterFavorites(documentsByID)).toEqual([]);
    });

    it('нет документов', () => {
        const documentsByID = new Map<string, ITreeItem>();
        expect(filterFavorites(documentsByID)).toEqual([]);
    });

});
