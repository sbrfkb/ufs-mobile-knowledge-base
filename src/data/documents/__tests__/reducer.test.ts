import {reducer} from '../reducer';
import * as actionTypes from '../actionTypes';
import * as treeActionTypes from '../../tree/actionTypes';
import {Group, ITreeItem} from '../../tree/models';
import {ExternalDocument, StaticDocument} from '../models';


describe('documents:reducer', () => {

    it('должен возвращать начальный стейт', () => {
        const action = {
            type: 'test action'
        };
        const initialState = {
            fetching: false,
            itemsByID: new Map<string, ITreeItem>()
        };

        expect(reducer(initialState, action)).toEqual(initialState);
    });

    it('UPDATE', () => {
        const documentsByID = new Map<string, ITreeItem>();


        const document1PDF = new ExternalDocument();
        document1PDF.id = 'dp1';
        document1PDF.path = 'document1.pdf';
        document1PDF.title = 'Document 1';
        documentsByID.set(document1PDF.id, document1PDF);

        const document2TXT = new StaticDocument();
        document2TXT.id = 'd2';
        document2TXT.path = 'document2.txt';
        document2TXT.title = 'Document 2';
        documentsByID.set(document2TXT.id, document2TXT);

        const group1 = new Group();
        group1.id = '1';
        group1.path = 'group1';
        group1.title = 'Group 1';
        documentsByID.set(group1.id, group1);

        const initialState = {
            fetching: false,
            itemsByID: documentsByID
        };

        document2TXT.content = 'New Content';
        documentsByID.set(document2TXT.id, document2TXT);

        const action = {
            type: actionTypes.UPDATE,
            payload: {
                item: document2TXT
            }
        };
        const expectedState = {
            fetching: false,
            itemsByID: documentsByID
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });


    it('RECEIVE', () => {
        const documentsByID = new Map<string, ITreeItem>();

        const document1PDF = new ExternalDocument();
        document1PDF.id = 'dp1';
        document1PDF.path = 'document1.pdf';
        document1PDF.title = 'Document 1';
        documentsByID.set(document1PDF.id, document1PDF);

        const document2TXT = new StaticDocument();
        document2TXT.id = 'd2';
        document2TXT.path = 'document2.txt';
        document2TXT.title = 'Document 2';
        documentsByID.set(document2TXT.id, document2TXT);

        const group1 = new Group();
        group1.id = '1';
        group1.path = 'group1';
        group1.title = 'Group 1';
        documentsByID.set(group1.id, group1);

        const initialState = {
            fetching: false,
            itemsByID: new Map<string, ITreeItem>()
        };

        const action = {
            type: actionTypes.RECEIVE,
            payload: {
                itemsByID: documentsByID
            }
        };
        const expectedState = {
            fetching: false,
            itemsByID: documentsByID
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    describe('LOAD_CONTENT_REQUEST', () => {

        it('BEGIN', () => {
            const initialState = {
                fetching: false,
                itemsByID: new Map<string, ITreeItem>()
            };
            const action = {
                type: actionTypes.LOAD_CONTENT_REQUEST.BEGIN
            };
            const expectedState = {
                fetching: true,
                itemsByID: initialState.itemsByID
            };

            expect(reducer(initialState, action)).toEqual(expectedState);
        });

        it('END', () => {
            const initialState = {
                fetching: true,
                itemsByID: new Map<string, ITreeItem>()
            };
            const action = {
                type: actionTypes.LOAD_CONTENT_REQUEST.END
            };
            const expectedState = {
                fetching: false,
                itemsByID: initialState.itemsByID
            };

            expect(reducer(initialState, action)).toEqual(expectedState);
        });

        it('FAILURE', () => {
            const initialState = {
                fetching: true,
                itemsByID: new Map<string, ITreeItem>()
            };
            const error = new Error('error load content');
            const action = {
                type: actionTypes.LOAD_CONTENT_REQUEST.FAILURE,
                payload: {
                    error
                }
            };
            const expectedState = {
                fetching: false,
                itemsByID: initialState.itemsByID,
                error
            };

            expect(reducer(initialState, action)).toEqual(expectedState);
        });

    });

    it('SELECT TREE ITEM', () => {
        const initialState = {
            fetching: true,
            itemsByID: new Map<string, ITreeItem>()
        };
        const action = {
            type: treeActionTypes.SELECT,
            payload: {
                id: '1'
            }
        };
        const expectedState = {
            fetching: false,
            itemsByID: initialState.itemsByID
        };

        expect(reducer(initialState, action)).toEqual(expectedState);
    });

});
