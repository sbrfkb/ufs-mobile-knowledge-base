import { combineReducers } from 'redux';
import { reducer as treeReducer } from './tree/reducer';
import { reducer as documentsReducer } from './documents/reducer';
export const reducer = combineReducers({
    tree: treeReducer,
    documents: documentsReducer
});
//# sourceMappingURL=reducer.js.map