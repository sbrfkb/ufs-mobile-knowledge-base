import {IDataState} from '../data/models';
import {IScreensState} from '../screens/models';


export interface IGlobalState {
    splitPanels: any
    user: {
        knowledgeBase: {
            data: IDataState,
            screens: IScreensState
        }
    }
}


export interface IAction {
    type: string
    payload?: any
}
