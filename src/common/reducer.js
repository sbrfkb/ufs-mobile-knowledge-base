import { combineReducers } from 'redux';
import { reducer as dataReducer } from '../data/reducer';
import { reducer as screensReducer } from '../screens/reducer';
export const reducer = combineReducers({
    data: dataReducer,
    screens: screensReducer
});
//# sourceMappingURL=reducer.js.map