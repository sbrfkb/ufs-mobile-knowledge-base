export const colors = {
    black: '#2F2F2F',
    blue: '#007aff',
    green: '#2BC33E',
    orange: '#F4A21C',
    placeholder: '#919191',
    border: '#cccccc',
    grayBG: 'rgb(248,248,248)',
    darkGrayBG: '#E6E6EB',
    darkBG: 'rgb(47,47,47)',
    highlight: 'gray'
};
export const fonts = {
    regular: 'PTSans-Regular',
    bold: 'PTSans-Bold'
};
export const apiSource = 'ecm';
//# sourceMappingURL=constants.js.map